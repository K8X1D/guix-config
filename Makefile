# Inspirations:
# - https://codeberg.org/SystemCrafters/crafted-guix

nix:
	@sudo herd restart nix-daemon	
	@nix-env -if bin/packages.nix
	ln -fs $$HOME/.nix-profile/share/applications/* $$HOME/.local/share/applications/

home:
	guix home reconfigure k8x1d/home.scm

home-dry:
	guix home reconfigure --dry-run k8x1d/home.scm

system-dry:
	@sudo guix system reconfigure --dry-run --load-path=. k8x1d/system.scm

system:
	@sudo guix system reconfigure --load-path=. k8x1d/system.scm

update_nix:
	@sudo herd restart nix-daemon	
	@nix-channel --update

update_guix:
	@guix pull --channels=channels/my-channels.scm
	@guix describe --format=channels > channels/channels.scm

set-up:
	@ln -fs "/nix/var/nix/profiles/per-user/$$USER/profile" ~/.nix-profile
	@nix-channel --add https://nixos.org/channels/nixpkgs-unstable
	@nix-channel --add https://github.com/guibou/nixGL/archive/main.tar.gz
	@source /run/current-system/profile/etc/profile.d/nix.sh

collect_garbage:
	@guix gc --collect-garbage

delete_generations:
	@guix gc --delete-generations
