{ pkgs ? import <nixpkgs> {} }:

[
  pkgs.appimage-run 
  pkgs.brave
  pkgs.dmg2img
  pkgs.nodePackages.dockerfile-language-server-nodejs
  pkgs.intel-gpu-tools
  pkgs.nodePackages.unified-language-server
  pkgs.languagetool
  pkgs.fasttext
  pkgs.lua-language-server
  pkgs.openvpn
  pkgs.scrcpy
  pkgs.stylua
  pkgs.libreoffice
  pkgs.lswt
  pkgs.luajitPackages.luacheck
  pkgs.nodePackages.bash-language-server
  pkgs.nodePackages.textlint
  pkgs.nodePackages.textlint-plugin-latex
  pkgs.nodePackages.textlint-rule-max-comma
  pkgs.nodePackages.textlint-rule-write-good
  pkgs.nodePackages.textlint-rule-stop-words
  pkgs.nodePackages.textlint-rule-diacritics
  pkgs.nodePackages.textlint-rule-terminology
  pkgs.nodePackages.textlint-rule-en-max-word-count
  pkgs.nodePackages.textlint-rule-unexpanded-acronym
  pkgs.nodePackages.textlint-rule-period-in-list-item
  pkgs.nodePackages.textlint-rule-common-misspellings
  pkgs.nodePackages.textlint-rule-abbr-within-parentheses
  pkgs.nodePackages.textlint-rule-no-start-duplicated-conjunction
  pkgs.protonmail-bridge
  pkgs.ltex-ls
  pkgs.system76-keyboard-configurator
  pkgs.thunderbird
  pkgs.zotero_7
]


