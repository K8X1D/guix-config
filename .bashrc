# Bash initialization for interactive non-login shells and
# for remote shells (info "(bash) Bash Startup Files").

# Export 'SHELL' to child processes.  Programs such as 'screen'
# honor it and otherwise use /bin/sh.
export SHELL

if [[ $- != *i* ]]
then
    # We are being invoked from a non-interactive shell.  If this
    # is an SSH session (as in "ssh host command"), source
    # /etc/profile so we get PATH and other essential variables.
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile

    # Don't do anything else.
    return
fi

# Source the system-wide file.
source /etc/bashrc

# Aliases
alias ls='ls -p --color=auto'
alias ll='ls -l'
alias grep='grep --color=auto'



# Set prompt base (start)
# from https://thucnc.medium.com/how-to-show-current-git-branch-with-colors-in-bash-prompt-380d05a24745
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

PS1="\u@\h \[\e[32m\]\w \[\e[91m\]\$(parse_git_branch)\[\e[00m\]\n"




# History ####
# enable completion
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

# To remove all but the last identical command, and commands that start with a space: 
export HISTCONTROL="ignoreboth:erasedups"

# Increase size
export HISTSIZE=10000
export HISTFILESIZE=2000000



# Completion ####
# add completion to sudo
#complete -cf sudo
# conflict with bash-completion

#
# vterm
#
# see https://github.com/akermu/emacs-libvterm#shell-side-configuration
vterm_printf() {
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ]); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

# directory tracking
vterm_prompt_end(){
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
}
PS1=$PS1'\[$(vterm_prompt_end)\]'

# Adjust the prompt depending on whether we're in 'guix environment'.
if [ -n "$GUIX_ENVIRONMENT" ]
then
    PS1=$PS1'(guix env) '
fi

# Set prompt base (end)
PS1=$PS1'\$ '

# Use bash-completion, if available
[[ $PS1 && -f $HOME/.guix-home/profile/share/bash-completion/bash_completion ]] && \
   . $HOME/.guix-home/profile/share/bash-completion/bash_completion

# zsh-like completion
# see https://superuser.com/questions/288714/bash-autocomplete-like-zsh
bind 'set show-all-if-ambiguous on'
bind 'TAB:menu-complete'

