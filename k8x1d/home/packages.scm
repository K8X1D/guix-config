;; TODO: Unify package definition
(define-module (k8x1d home packages)
  #:use-module (gnu packages android)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages rdesktop)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages video)
  #:use-module (gnu packages vpn)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages)
  #:use-module (guix profiles)
  #:use-module (k8x1d packages R)
  #:use-module (k8x1d packages building-tools)
  #:use-module (k8x1d packages desktop)
  #:use-module (k8x1d packages fonts)
  #:use-module (k8x1d packages remote)
  #:use-module (k8x1d packages julia)
  #:use-module (k8x1d packages latex)
  #:use-module (k8x1d packages multimedia)
  #:use-module (k8x1d packages neovim)
  #:use-module (k8x1d packages plasma-desktop)
  #:use-module (k8x1d packages python)
  #:use-module (k8x1d packages secrets)
  #:use-module (k8x1d packages shells)
  #:use-module (k8x1d packages browsers)
  #:use-module (k8x1d packages sound)
  #:use-module (k8x1d packages virtualization)
  #:use-module (nongnu packages messaging)
  #:export (%k8x1d-packages))


(define-public %k8x1d-dwl
  (specifications->packages
   (list
    "poppler" ;; PDF rendering library 
    "qpdf" ;; Command-line tools and library for transforming PDF files 
    "wlogout" ;; Logout menu for Wayland
    ;; "telegram-desktop" ;; Telegram Desktop
    "signal-desktop" ;; Private messenger using the Signal protocol
    "rofi-wayland"
    "alacritty"
    "xdg-utils" ;; Freedesktop.org scripts for desktop integration
    "light"
    "direnv"
    "swaybg"
    "dunst"
    "redshift-wayland"
    "swayidle"
    "bemenu"
    "cups-minimal"
    "rofi-pass"
    "node" ;; Evented I/O for V8 JavaScript
    "tlp"
    "unzip"
    "yt-dlp" ;; Download videos from YouTube.com and other sites
    "bluez"
    "htop"
    "wl-clipboard" ;; Command-line copy/paste utilities for Wayland
    )))


(define-public %k8x1d-system-management
  (specifications->packages
   (list
    "net-tools" ;; Tools for controlling the network subsystem in Linux
    )))

(define-public %k8x1d-power-management
  (specifications->packages
   (list
    "tlp" ;; Power management tool for Linux
    ;; "tlpui" ;; User interface for TLP written in Python
    "powertop" ;; Analyze power consumption on Intel-based laptops
    )))

(define-public %k8x1d-guile
  (specifications->packages
   (list
    "guile-json" ;; JSON module for Guile
    )))


(define-public %k8x1d-wayland
  (specifications->packages
   (list
    "xlsclients" ;; List client applications running on a display
    "qtwayland@5" ;; Qt Wayland module, incompatible with dwl config
    ;; "qtwayland" ;; Qt Wayland module
    )))

(define-public %k8x1d-xorg
  (specifications->packages
   (list
    "feh" ;; Fast and light imlib2-based image viewer
    "picom" ;; Compositor for X11, forked from Compton
    "xset" ;; User preference utility for X server
    )))

(define-public %k8x1d-backup
  (specifications->packages
   (list
    "rsync" ;; Remote (and local) file copying tool
    )))

(define-public %k8x1d-packages
  (append 
   %k8x1d-python
   %k8x1d-julia
   %k8x1d-latex
   %k8x1d-dwl
   %k8x1d-R
   %k8x1d-guile
   %k8x1d-sound
   %k8x1d-fonts
   %k8x1d-building-tools
   %k8x1d-virtualization
   %k8x1d-power-management
   %k8x1d-wayland
   %k8x1d-xorg
   %k8x1d-neovim
   %k8x1d-secrets
   %k8x1d-shells
   %k8x1d-remote
   %k8x1d-browsers
   %k8x1d-desktop
   %k8x1d-multimedia
   %k8x1d-backup
   %k8x1d-system-management
   ))
