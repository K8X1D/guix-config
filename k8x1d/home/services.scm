(define-module (k8x1d home services)
  #:use-module (guix gexp)
  #:use-module (gnu services)
  #:use-module (gnu home services) 
  #:use-module (gnu home services shells)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu home services gnupg)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services sound)
  #:use-module (gnu home services pm)
  #:use-module (gnu packages bittorrent)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages wm)
  #:export (%k8x1d-services))

(define %logdir
  (string-append
   (or (getenv "XDG_STATE_HOME")
       (string-append (getenv "HOME") "/.local/state"))
   "/log"))

(define-public %k8x1d-shells-services
  (list
   (service home-zsh-service-type
	    (home-zsh-configuration
	     (zshrc (list (local-file
			   "/home/k8x1d/Documents/Developpement/Logiciels/frame14/guix-config/configs/zsh/.zshrc"
			   "zshrc")))
	     (zprofile (list (local-file
			      "/home/k8x1d/Documents/Developpement/Logiciels/frame14/guix-config/configs/zsh/.zprofile"
			      "zprofile")))
	     (zshenv (list (local-file
			    "/home/k8x1d/Documents/Developpement/Logiciels/frame14/guix-config/configs/zsh/.zshenv"
			    "zshenv")))
	     ))
   ))

(define-public %k8x1d-gpg-services
  (list
   (service home-gpg-agent-service-type
	    (home-gpg-agent-configuration
	     (pinentry-program
	      (file-append pinentry-gnome3 "/bin/pinentry-gnome3")
	      )
	     (ssh-support? #t)
	     (extra-content "allow-loopback-pinentry")
	     ))
   ))

(define-public %k8x1d-configs
  (list
   (simple-service 'mpd-config
		   home-xdg-configuration-files-service-type
		   `(("mpd/mpd.conf" ,(local-file "/home/k8x1d/Documents/Developpement/Logiciels/frame14/guix-config/configs/mpd/mpd.conf"))))
   ))

(define-public %k8x1d-shepherd-services
  (list
   (service home-shepherd-service-type
	    (home-shepherd-configuration
	     (auto-start? #t)
	     (services (list
			;; (shepherd-service
			;;  (documentation "Notification daemon.")
			;;  (provision '(dunst))
			;;  (one-shot? #t)
			;;  (start #~(make-forkexec-constructor
			;; 	   (list #$(file-append dunst "/bin/dunst"))
			;; 	   #:log-file (string-append #$%logdir "/dunst.log")))
			;;  (stop #~(make-kill-destructor)))

			;; Music player daemon
			 (shepherd-service
			  (documentation "Music Player Daemon.")
			  (provision '(mpd))
			  (one-shot? #t)
			  (start #~(make-forkexec-constructor
			 	   (list #$(file-append mpd "/bin/mpd"))
			 	   ))
			  (stop #~(make-kill-destructor)))

			;; File sharing
			(shepherd-service
			 (documentation "Fast and easy BitTorrent client.")
			 (provision '(transmission))
			 (start #~(make-forkexec-constructor
				   (list #$(file-append transmission "/bin/transmission-daemon") "--foreground" "--download-dir=/extension/Multimedia/Torrents")
				   #:log-file (string-append #$%logdir "/transmission.log")))
			 (stop #~(make-forkexec-constructor
				  (list #$(file-append transmission "/bin/transmission-remote") "--exit")
				  ))
			 (auto-start? #f)
			 (respawn? #f))
			))
	     ))

   ))

(define-public %k8x1d-sound-services
  (list
   (service home-pipewire-service-type
	    (home-pipewire-configuration
	     (enable-pulseaudio? #t)
	    ))
   ))

(define-public %k8x1d-desktop-services
  (list
   (simple-service 'some-useful-env-vars-service
		   home-environment-variables-service-type
		   `(
		     ("GTK_THEME" . "Breeze-Dark")
		     ("SHELL" . ,(file-append zsh "/bin/zsh"))
		     ("QT_QPA_PLATFORM" . "wayland:xcb:wayland-egl")
		     ;; Available platform plugins are: eglfs, linuxfb, minimal, minimalegl, offscreen, vnc, xcb.
		     ("PATH" . "$HOME/.local/bin:$HOME/.nix-profile/bin:$PATH")
		     ("XDG_LOG_HOME" . "$HOME/.local/state/log/")
		     ("XDG_DATA_DIRS" . "$HOME/.local/share/flatpak/exports/share:$XDG_DATA_DIRS")
		     ("QT_QPA_PLATFORMTHEME" . "qt5ct")
		     )
		   )
   (service home-dbus-service-type)
   (service home-batsignal-service-type
	    (home-batsignal-configuration
	     (notifications? #t)
	     (critical-level 7)
	     (danger-level 4)
	     (danger-command "loginctl hibernate")
	     ))
   ))

(define-public %k8x1d-services
  (append
   %k8x1d-shells-services
   %k8x1d-gpg-services
   %k8x1d-shepherd-services
   %k8x1d-configs
   %k8x1d-sound-services
   %k8x1d-desktop-services
   ))
