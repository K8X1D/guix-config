(define-module (k8x1d packages sound)
  #:use-module (gnu packages)
  #:use-module (gnu packages video)
  #:use-module (gnu packages music)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages pulseaudio)
  #:export (%k8x1d-sound))

(define-public %k8x1d-sound 
   (specifications->packages
    (list
     "pipewire"
     "wireplumber"
     ;;"mpv-mpris" ;; MPRIS plugin for mpv
     "playerctl" ;; Control MPRIS-supporting media player applications
     "mpdris2" ;; MPRIS V2.1 support for MPD 
     "mpd-mpc" ;; Music Player Daemon client 
     "mpd" ;; Music Player Daemon
     "pavucontrol" ;; PulseAudio Volume Control
     )))
