(define-module (k8x1d packages R)
  #:use-module (gnu packages)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages textutils)
  #:use-module (guix-science packages cran)
  #:use-module (guix-science packages rstudio)
  #:use-module (guix-cran packages p)
  #:export (%k8x1d-R))

(define-public %k8x1d-R
   (specifications->packages
    (list
     ;; Distribution
     "r" ;; Environment for statistical computing and graphics

     ;; Packages
     "r-languageserver" ;; Language Server for R
     "r-lintr" ;; Linter for R code
     "r-renv" ;; Project environments
     "r-here" ;; Simpler way to find files
     "r-readtext" ;; Import and Handling for Plain and Formatted Text Files
     "r-dplyr" ;; Tools for working with data frames in R
     "r-tidyr" ;; Tidy data with `spread()` and `gather()` functions
     "r-foreach" ;; Foreach looping construct for R
     "r-ggplot2" ;; Implementation of the grammar of graphics
     "r-doparallel" ;; Foreach parallel adaptor for the 'parallel' package
     "r-openxlsx" ;; Read, write and edit XLSX files
     "r-rlog" ;; Simple, Opinionated Logging Utility
     "r-rmarkdown" ;; Convert R Markdown documents into a variety of formats
     "r-rstatix" ;; Pipe-friendly framework for basic statistical tests 
     "r-ggpubr" ;; ggplot2-based publication-ready plots
     "r-datarium" ;; Data Bank for Statistical Analysis and Visualization
     "r-palmerpenguins" ;; Palmer Archipelago (Antarctica) penguin data
     ;; "r-report" ;; Automated Reporting of Results and Statistical Models
     "r-multcomp" ;; Simultaneous inference in general parametric models  
     "r-flexmix" ;; Flexible mixture modeling
     "r-broom" ;; Convert statistical analysis objects into tidy data frames

     
     ;; FIXME: hang on compilation...
     ;; "r-xlsx" ;; Read, write, format Excel 2007 and Excel 97/2000/XP/2003 files

     ;; Dependencies
     "openssl" ;; SSL/TLS implementation
     "openjdk" ;; Java development kit
     "curl" ;; Command line tool for transferring data with URL syntax  
     "poppler" ;; PDF rendering library
     "pkg-config" ;; Text Extraction, Rendering and Converting of PDF Documents
     "antiword" ;; Microsoft Word document reader 

     ;; IDE (for testing purpos)
     "rstudio" ;; Integrated development environment (IDE) for R (desktop version)
     "r-reticulate" ;; R interface to Python
     "r-pylintr" ;; Lint 'Python' Files with a R Command or a 'RStudio' Addin
     "r-miniui" ;; Shiny UI widgets for small screens

     ;; Notebook
     "r-irkernel" ;; Native R kernel for Jupyter
     )))
