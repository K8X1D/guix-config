;; see https://guix.gnu.org/manual/en/html_node/Using-TeX-and-LaTeX.html
(define-module (k8x1d packages latex)
  #:use-module (gnu packages)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages texlive)
  #:use-module (gnu packages perl)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build-system texlive)
  #:use-module (guix licenses)
  #:export (%k8x1d-latex))



(define-public texlive-arydshln
  (package
   (name "texlive-arydshln")
   (version (number->string %texlive-revision))
   (source (texlive-origin name version
			   (list "doc/latex/arydshln/" "source/latex/arydshln/"
				 "tex/latex/arydshln/")
			   (base32
			    "0zhn8fq92ghkci457qmls90yd2q55zfgqd6rxyhzl5nsfhamcrvh")))
   (outputs '("out" "doc"))
   (build-system texlive-build-system)
   (home-page "https://ctan.org/pkg/arydshln")
   (synopsis "Draw dash-lines in array/tabular")
   (description
    "The package is to draw dash-lines in array/tabular environments.  Horizontal
lines are drawn by \\hdashline and \\cdashline while vertical ones can be
specified as a part of the preamble using :'.  The shape of dash-lines may be
controlled through style parameters or optional arguments.  The package is
compatible with array, colortab, longtable, and colortbl.")
   (license lppl1.0+))
  )


(define-public %k8x1d-latex
  (append
   (specifications->packages
    (list
     "texlive-babel-french" ;; Babel contributed support for French
     "texlive-beamer" ;; LaTeX class for producing presentations and slides
     "texlive-biber" ;; BibTeX replacement for users of BibLaTeX
     "texlive-biblatex" ;; Sophisticated Bibliographies in LaTeX
     "texlive-biblatex-apa" ;; BibLaTeX citation and reference style for APA
     "texlive-booktabs" ;; Publication quality tables in LaTeX
     "texlive-caption" ;; Customising captions in floating environments
     "texlive-chktex" ;; Check for errors in LaTeX documents
     "texlive-collection-fontsrecommended" ;; Recommended fonts
     "texlive-collection-langfrench" ;; Support for French and Basque
     "texlive-csquotes" ;; Context sensitive quotation facilities
     "texlive-digestif" ;; Editor plugin for LaTeX, ConTeXt etc.
     "texlive-dvipng" ;; DVI to PNG/GIF converter
     "texlive-e-french" ;; Comprehensive LaTeX support for French-language typesetting
     "texlive-enumitem" ;; Control layout of itemize, enumerate, description
     "texlive-epigraph" ;; Typeset epigraphs
     "texlive-luatex" ;; Extended version of pdfTeX using Lua
     "texlive-makecell" ;; Tabular column heads and multilined cells
     "texlive-multirow" ;; Create tabular cells spanning multiple rows
     "texlive-nextpage" ;; Generalisations of the page advance commands
     "texlive-pgf" ;; Create PostScript and PDF graphics in TeX
     "texlive-pgfplots" ;; Create normal/logarithmic plots in two and three dimensions
     "texlive-scheme-basic" ;; Basic scheme (plain and latex)
     "texlive-scripts" ;; TeX Live infrastructure programs
     "texlive-setspace" ;; Set space between lines
     "texlive-subfigure" ;; Deprecated: Figures divided into subfigures
     "texlive-tikz-dependency" ;; Library for drawing dependency graphs
     "texlive-times" ;; Replacement for Adobe's Times font
     "texlive-was" ;; Collection of small packages by Walter Schmidt
     "texlive-xcolor" ;; Driver-independent color extensions for LaTeX and pdfLaTeX
     "texlive-subfig" ;; Figures broken into subfigures
     "texlive-enumitem" ;; Control layout of itemize, enumerate, description
     ;; "texlive-enumitem-zref" ;; Extended references to items for `enumitem' package (broken: https://ctan.org/pkg/enumitem-zref)
     "texlive-hyperref" ;; Extensive support for hypertext in LaTeX  
     "texlive-tools" ;; The LaTeX standard tools bundle
     "texlive-ulem" ;; Package for underlining
     ))
   (list
    texlive-arydshln ;; Draw dash-lines in array/tabular
    )
   )
  )
