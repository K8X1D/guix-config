
(define-module (k8x1d packages plasma-desktop)
  #:use-module (gnu packages kde-frameworks)
  #:export (%k8x1d-plasma-desktop))

(define-public %k8x1d-plasma-desktop
   (list
    "bluedevil" ;; Manage the Bluetooth settings from Plasma
    "bluez-qt" ;; QML wrapper for BlueZ
    "plasma-pass" ;; Plasma applet for the Pass password manager
    "plasma-nm" ;; Plasma applet for managing network connections
    ))













