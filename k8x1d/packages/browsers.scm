(define-module (k8x1d packages browsers)
  #:use-module (gnu packages)
  #:use-module (gnu packages chromium)
  #:use-module (gnu build chromium-extension)
  #:use-module (gnu packages web-browsers)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages rust-apps)
  #:use-module (nongnu packages mozilla)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:export (%k8x1d-browsers))

(define-public python-adblock
  (package
    (name "python-adblock")
    (version "0.6.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "adblock" version))
       (sha256
        (base32 "0wjfaqmyhgijvklkgx40bcjifiw5cpv4sx842ibyxcv9djaiwr8i"))))
    (build-system pyproject-build-system)
    (inputs (list maturin))
    (home-page "https://github.com/ArniDagur/python-adblock")
    (synopsis "")
    (description "")
    (license #f)))

(define-public %k8x1d-browsers
  (append
   (specifications->packages
    (list
     "ungoogled-chromium-wayland" ;; Graphical web browser
     "ublock-origin-chromium" ;; Block unwanted content from web sites
     "nyxt" ;; Extensible web-browser in Common Lisp
     "gst-libav" ;; GStreamer plugins and helper libraries
     "gst-plugins-bad" ;; Plugins for the GStreamer multimedia library
     "gst-plugins-base" ;; GStreamer plugins and helper libraries
     "gst-plugins-good" ;; GStreamer plugins and helper libraries
     "gst-plugins-ugly" ;; GStreamer plugins and helper libraries
     "firefox"
     "qutebrowser" ;; Minimal, keyboard-focused, vim-like web browser
     ))
   (list
    ;; python-adblock ;; FIXME: don't build, maturin failel  Caused by: The following metadata fields in `package.metadata.maturin` section of Cargo.toml are removed since maturin 0.14.0: classifier, requires-python, please set them in pyproject.toml as PEP 621 specifies.
    )))
