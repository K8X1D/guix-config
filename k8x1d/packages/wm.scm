(define-module (k8x1d packages wm)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (dwl-guile home-service)
  #:use-module (dwl-guile packages)
  #:use-module (dwl-guile patches)
  #:use-module ((guix licenses) #:prefix license:)
  )

(define-public %ipc-v2-fixed-patch
  (origin (method url-fetch)
          (uri "https://gist.githubusercontent.com/fbushstone/b116c44340eb7a7878de1119dd931ca5/raw/ee66ac9e2a5dddd9b528df553e21080c2811e974/ipc-v2-fixed.patch")
          (sha256 (base32 "19rkkc2ilsssnfig0va8rw59x6hc5bnv5x0ai9vpqgp3lf2yxh2d"))
          (file-name "ipc-v2-fixed-patch")
	  ))

(define-public %autostart-patch
  (origin (method url-fetch)
          (uri "https://github.com/djpohly/dwl/compare/main...nakibrayan3:dwl:update-autostart-patch.patch")
          (sha256 (base32 "0khmshsfifdgca2jh84b93alcipwzzjp9w4s236gxxi75arhpcb0"))
          (file-name "autostart-patch")
	  ))

(define-public %alwayscenter-patch
  (origin (method url-fetch)
          (uri "https://github.com/djpohly/dwl/compare/main...mortezadadgar:alwayscenter.patch")
          (sha256 (base32 "0zi2p04wz7fm836wyqmx7gkwn2arjs3xw3h9dfi0sk6c0h9c4lmw"))
          (file-name "alwayscenter-patch")
	  ))

(define-public %fibonacci-patch
  (origin (method url-fetch)
          (uri "https://github.com/djpohly/dwl/compare/main...Abanoub8:fibonacci.patch")
          (sha256 (base32 "0sx91xx131jk630msvyfi1p0glnfdfzay7468b1fillhsh457k7y"))
          (file-name "fibonacci-patch")
	  ))

;; https://codeberg.org/dwl/dwl/issues/529
(define-public k8x1d-dwl
  (let ((commit "b4da97446aafba04ac10062b16f343ac95a81e90"))
    (package
      (name "k8x1d-dwl")
      (version (string-append "0.5." (string-take commit 7)))
      (source (origin
		(method git-fetch)
		(uri (git-reference
		      (url "https://github.com/djpohly/dwl")
		      (commit commit)))
		(patches (list ;;%ipc-v2-fixed-patch
			  ;; %autostart-patch
			  %alwayscenter-patch
			  ;; %fibonacci-patch
			  ))
		(file-name (git-file-name name version))
		(sha256
		 (base32
                  "01nyvbm8xxwpa44cc88y142xd3zm58692gik6bcwi69zrg3wn9lj"))))
      (build-system gnu-build-system)
      (arguments
       `(#:tests? #f                      ; no tests
	 #:make-flags
	 (list
	  (string-append "XWAYLAND=" "-DXWAYLAND") 
	  (string-append "XLIBS=" "xcb xcb-icccm")
          (string-append "CC=" ,(cc-for-target))
          (string-append "PREFIX=" (assoc-ref %outputs "out")))
	 #:phases
	 (modify-phases %standard-phases
	   (delete 'configure)
	   (add-after 'build 'install-wayland-sessions
	     (lambda* (#:key outputs #:allow-other-keys)
	       ;; Add a .desktop file to wayland-sessions.
	       (let* ((output (assoc-ref outputs "out"))
		      (wayland-sessions (string-append output "/share/wayland-sessions")))
		 (mkdir-p wayland-sessions)
		 (with-output-to-file
		     (string-append wayland-sessions "/dwl.desktop")
		   (lambda _
		     (format #t
			     "[Desktop Entry]~@
		     Name=dwl~@
		     Comment=Dynamic Window Manager for Wayland~@
		     Exec=/home/k8x1d/.local/bin/dwl_start~@
		     TryExec=~@*~a/bin/dwl~@
		     Icon=~@
		     Type=Application~%"
			     output)))
		 #t)))
	   )))
      (native-inputs
       (list pkg-config))
      (inputs
       (list wlroots wayland-protocols xorg-server-xwayland))
      (home-page "https://github.com/djpohly/dwl")
      (synopsis "Dynamic window manager for Wayland")
      (description
       "@command{dwl} is a compact, hackable compositor for Wayland based on
wlroots.  It is intended to fill the same space in the Wayland world that dwm
does in X11, primarily in terms of philosophy, and secondarily in terms of
functionality.  Like dwm, dwl is easy to understand and hack on, due to a
limited size and a few external dependencies.  It is configurable via
@file{config.h}.")
      ;;             LICENSE       LICENSE.dwm   LICENSE.tinywl
      (license (list license:gpl3+ license:expat license:cc0)))))


;; FIXME: /var/log/guix/drvs/5q/iaimcyyspl37pd3hyghd8bl14c2a2j-wbg-1.1.423f7dc.drv.gz
;;(define-public wbg
;;  (let ((commit "423f7dc6990b1cb5afde95f8383c44b1a4dce16a"))
;;    (package
;;      (name "wbg")
;;      (version (string-append "1.1." (string-take commit 7)))
;;      (source (origin
;;		(method git-fetch)
;;		(uri (git-reference
;;		      (url "https://codeberg.org/dnkl/wbg")
;;		      (commit commit)))
;;		(file-name (git-file-name name version))
;;		(sha256
;;		 (base32
;;		  "1jav07465ifzb74s5hfci3s6b18rz74zfnlm1nfnvnwj42dv1p9d"))))
;;    (build-system meson-build-system)
;;      ;;(arguments
;;      ;; `(#:tests? #f                      ; no tests
;;      ;;	 #:phases
;;      ;;	 (modify-phases %standard-phases
;;      ;;	   (delete 'configure))))
;;      (native-inputs
;;       (list pkg-config ninja cmake libpng libjpeg libwebp))
;;      (inputs (list pixman wayland wayland-protocols tllist))
;;    (home-page "https://codeberg.org/dnkl/wbg")
;;    (synopsis "Super simple wallpaper application for Wayland compositors implementing the layer-shell protocol")
;;    (description "Super simple wallpaper application for Wayland compositors implementing the layer-shell protocol.
;;
;;Wbg takes a single command line argument: a path to an image file. This image is displayed scaled-to-fit on all monitors.
;;
;;More display options, and/or the ability to set a per-monitor wallpaper may be added in the future.
;;
;;Packaging status.")
;;    (license license:expat))))


(define-public k8x1d-dwl-guile-test
  (package
   (inherit k8x1d-dwl)
   (name "dwl-guile")
   (version "2.0.2")
   (inputs
    (modify-inputs (package-inputs k8x1d-dwl)
		   (prepend guile-3.0)
		   ))
   (arguments
    `(#:tests? #f                      ; no tests
      #:make-flags
      (list
       (string-append "CC=" ,(cc-for-target))
       (string-append "PREFIX=" (assoc-ref %outputs "out")))
      #:phases
      (modify-phases %standard-phases
	(delete 'configure) ;; no configure
	(add-after 'build 'install-wayland-sessions
	  (lambda* (#:key outputs #:allow-other-keys)
	    ;; Add a .desktop file to wayland-sessions.
	    (let* ((output (assoc-ref outputs "out"))
		   (wayland-sessions (string-append output "/share/wayland-sessions")))
	      (mkdir-p wayland-sessions)
	      (with-output-to-file
		  (string-append wayland-sessions "/dwl-guile.desktop")
		(lambda _
		  (format #t
			  "[Desktop Entry]~@
		     Name=dwl-guile~@
		     Comment=Dynamic Window Manager for Wayland configured in Guile Scheme~@
		     Exec=/home/k8x1d/.local/bin/dwl-guile_start~@
		     TryExec=~@*~a/bin/dwl-guile~@
		     Icon=~@
		     Type=Application~%"
			  output)))
	      #t)))
	)))
   (source
    (origin
     (inherit (package-source k8x1d-dwl))
     (uri (git-reference
           (url "https://github.com/engstrand-config/dwl-guile")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0z8ls92cgkry0fvybqzvg8jplwh3zjakdmq79kfxbczabvyijxk8"))))))


(define-public k8x1d-dwl-guile
  (package
   (inherit dwl-guile)
   (name "k8x1d-dwl-guile")
   (arguments
    `(#:tests? #f                      ; no tests
      #:make-flags
      (list
       (string-append "CC=" ,(cc-for-target))
       (string-append "PREFIX=" (assoc-ref %outputs "out")))
      #:phases
      (modify-phases %standard-phases
		     (delete 'configure) ;; no configure
		     (add-after 'build 'install-wayland-sessions
				(lambda* (#:key outputs #:allow-other-keys)
				  ;; Add a .desktop file to wayland-sessions.
				  (let* ((output (assoc-ref outputs "out"))
					 (wayland-sessions (string-append output "/share/wayland-sessions")))
				    (mkdir-p wayland-sessions)
				    (with-output-to-file
					(string-append wayland-sessions "/dwl-guile.desktop")
				      (lambda _
					(format #t
						"[Desktop Entry]~@
		     Name=dwl-guile~@
		     Comment=Dynamic Window Manager for Wayland configured in Guile Scheme~@
		     Exec=/home/k8x1d/.local/bin/dwl-guile_start~@
		     TryExec=~@*~a/bin/dwl-guile~@
		     Icon=~@
		     Type=Application~%"
						output)))
				    #t)))
		     )))         

   ))


;;(define-public %patch-alwayscenter
;;  (origin (method url-fetch)
;;	  ;; (uri "https://github.com/djpohly/dwl/compare/main...mortezadadgar:alwayscenter.patch")
;;	  (uri "https://github.com/djpohly/dwl/compare/main...wochap:alwayscenter.patch")
;;          (sha256 (base32 "0zi2p04wz7fm836wyqmx7gkwn2arjs3xw3h9dfi0sk6c0h9c4lmw"))
;;          (file-name "alwayscenter-patch")
;;	  ))


(define-public %patch-alwayscenter
  (local-file "../../configs/dwl-guile/patches/alwayscenter.patch" #:recursive? #t))

(define-public %patch-regexrules
  (local-file "../../configs/dwl-guile/patches/regexrules.patch" #:recursive? #t))

(define-public %patch-pertag
  (local-file "../../configs/dwl-guile/patches/pertag.patch" #:recursive? #t))

;; in test
(define-public k8x1d-dwl-guile-xwayland
  (patch-dwl-guile-package k8x1d-dwl-guile
			   #:patches (list
				      ;; %patch-pertag ;; break other layout
					   %patch-alwayscenter
					   %patch-regexrules
					   %patch-xwayland
					   %patch-focusmonpointer)))
