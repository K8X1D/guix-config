(define-module (k8x1d packages neovim)
  #:use-module (gnu packages)
  #:use-module (gnu packages vim)
  #:export (%k8x1d-neovim))

(define-public %k8x1d-neovim
  (specifications->packages
   (list
    "neovim" ;; Fork of vim focused on extensibility and agility
    "eovim" ;; EFL GUI for Neovim
    )))
