(define-module (k8x1d packages multimedia)
  #:use-module (gnu packages)
  #:use-module (gnu packages video)
  #:use-module (gnu packages pulseaudio)
  #:export (%k8x1d-multimedia))

(define-public %k8x1d-multimedia
  (specifications->packages
   (list
    "mpv" ;; Audio and video player
    "pulseaudio" ;; Sound server
    "ffmpeg" ;; Audio and video framework
    )))
