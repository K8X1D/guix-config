(define-module (k8x1d packages desktop)
  #:use-module (gnu packages)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages gimp)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages video)
  #:use-module (gnu packages sync)
  #:use-module (gnu packages image-viewers)
  #:use-module (nongnu packages editors)
  #:export (%k8x1d-desktop))

(define-public %k8x1d-desktop
  (specifications->packages
   (list
    ;; "grimshot" ;; Screenshot utility for the Sway window manager
    ;; "gimp" ;; GNU Image Manipulation Program ; transfered to flatpak
    "obs" ;; Live streaming software                   
    "obs-wlrobs" ;; OBS plugin for Wayland (wlroots) screen capture
    "vscodium" ;; Community-driven, freely-licensed binary distribution of VSCode
    "okular" ;; Document viewer
    "pamixer" ;; PulseAudio command line mixer 
    "light" ;; GNU/Linux application to control backlights
    "nextcloud-client" ;; Desktop sync client for Nextcloud
    "libnotify" ;; GNOME desktop notification library
    ;; "gnome-keyring" ;; gnome-keyring
    "lm-sensors" ;; Utilities to read temperature/voltage/fan sensors
    "libsecret" ;; GObject bindings for "Secret Service" API
    "qtkeychain" ;; Qt API to store passwords
    "brightnessctl" ;; Backlight and LED brightness control
    "playerctl" ;; Control MPRIS-supporting media player applications
    "pamixer" ;; PulseAudio command line mixer
    "polkit-kde-agent" ;; Polkit authentication UI for Plasma 
    "kio-extras" ;; Additional components to increase the functionality of KIO
    "breeze-icons" ;; Default KDE Plasma 5 icon theme
    "oxygen-icons" ;; Oxygen provides the standard icon theme for the KDE desktop
    "hicolor-icon-theme"
    "breeze"
    "breeze-gtk"
    "qt5ct" ;; Qt5 Configuration Tool
    "flatpak" ;; System for building, distributing, and running sandboxed desktop applications
    ;; "fasttext" ;; Library for fast text representation and classification
    ;; "swaylock-effects"
    "cairo" ;; Multi-platform 2D graphics library
    "zip" ;; Compression and file packing utility
    "imv" ;; Image viewer for tiling window managers
    "docker-compose" ;; Multi-container orchestration for Docker
    "protonvpn-cli" ;; Command-line client for ProtonVPN
    )))
