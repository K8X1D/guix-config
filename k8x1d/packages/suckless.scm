(define-module (k8x1d packages suckless)
  #:use-module (dwl-guile home-service)
  #:use-module (dwl-guile packages)
  #:use-module (dwl-guile patches)
  #:use-module (gnu packages check)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages zig-xyz)
  #:use-module (gnu packages)
  #:use-module (guile)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build-system zig)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  )

(define-public k8x1d-dwm
  (package
    (inherit dwm)
    (name "k8x1d-dwm")
    (inputs (modify-inputs (package-inputs dwm)
              (append libxcb)
              (append imlib2)))
    (source (local-file "../../configs/dwm" #:recursive? #t))))

(define-public k8x1d-st
  (package
    (inherit st)
    (name "k8x1d-st")
     (source (local-file "../../configs/st" #:recursive? #t))))

(define-public k8x1d-slstatus
  (package
    (inherit slstatus)
    (name "k8x1d-slstatus")
    (source (local-file "../../configs/slstatus" #:recursive? #t))))


;;(define-public k8x1d-dwl
;;  (package
;;   (inherit dwl)
;;   (name "k8x1d-dwl")
;;   (source (local-file "../../configs/dwl" #:recursive? #t))
;;   (inputs (modify-inputs (package-inputs dwl)
;;			  (append wayland-protocols
;;				  xorg-server-xwayland)))
;;   (arguments
;;    `(#:tests? #f                      ; no tests
;;      #:validate-runpath? #f
;;      #:make-flags
;;      (list
;;       (string-append "CC=" ,(cc-for-target))
;;       (string-append "PREFIX=" (assoc-ref %outputs "out")))
;;      #:phases
;;      (modify-phases %standard-phases
;;		     (delete 'configure) ;; no configure
;;		     (add-after 'build 'install-wayland-sessions
;;				(lambda* (#:key outputs #:allow-other-keys)
;;				  ;; Add a .desktop file to wayland-sessions.
;;				  (let* ((output (assoc-ref outputs "out"))
;;					 (wayland-sessions (string-append output "/share/wayland-sessions")))
;;				    (mkdir-p wayland-sessions)
;;				    (with-output-to-file
;;					(string-append wayland-sessions "/dwl.desktop")
;;				      (lambda _
;;					(format #t
;;						"[Desktop Entry]~@
;;		     Name=dwl~@
;;		     Comment=Dynamic Window Manager for Wayland~@
;;		     Exec=/home/k8x1d/.local/bin/dwl_start~@
;;		     TryExec=~@*~a/bin/dwl~@
;;		     Icon=~@
;;		     Type=Application~%"
;;						output)))
;;				    #t)))
;;		     )))         
;;
;;   ))

;;(define-public k8x1d-dwl-guile
;;  (package
;;   (inherit dwl-guile)
;;   (name "k8x1d-dwl-guile")
;;   (arguments
;;    `(#:tests? #f                      ; no tests
;;      #:make-flags
;;      (list
;;       (string-append "CC=" ,(cc-for-target))
;;       (string-append "PREFIX=" (assoc-ref %outputs "out")))
;;      #:phases
;;      (modify-phases %standard-phases
;;		     (delete 'configure) ;; no configure
;;		     (add-after 'build 'install-wayland-sessions
;;				(lambda* (#:key outputs #:allow-other-keys)
;;				  ;; Add a .desktop file to wayland-sessions.
;;				  (let* ((output (assoc-ref outputs "out"))
;;					 (wayland-sessions (string-append output "/share/wayland-sessions")))
;;				    (mkdir-p wayland-sessions)
;;				    (with-output-to-file
;;					(string-append wayland-sessions "/dwl-guile.desktop")
;;				      (lambda _
;;					(format #t
;;						"[Desktop Entry]~@
;;		     Name=dwl-guile~@
;;		     Comment=Dynamic Window Manager for Wayland configured in Guile Scheme~@
;;		     Exec=/home/k8x1d/.local/bin/dwl-guile_start~@
;;		     TryExec=~@*~a/bin/dwl-guile~@
;;		     Icon=~@
;;		     Type=Application~%"
;;						output)))
;;				    #t)))
;;		     )))         
;;
;;   ))


(define %patch-alwayscenter
  (local-file "../../configs/dwl-guile/patches/alwayscenter.patch" #:recursive? #t))

(define %patch-regexrules
  (local-file "../../configs/dwl-guile/patches/regexrules.patch" #:recursive? #t))

(define %patch-pertag
  (local-file "../../configs/dwl-guile/patches/pertag.patch" #:recursive? #t))

 ;; in test
 (define-public k8x1d-dwl-guile-patched
    (patch-dwl-guile-package dwl-guile
 			     #:patches (list %patch-xwayland
					     %patch-alwayscenter
					     %patch-regexrules
					     %patch-focusmonpointer)))

(define-public k8x1d-river-with-desktop-entry
  (package
    (inherit river)
    (name "k8x1d-river-with-desktop-entry")
    (arguments
     (list
      #:zig-build-flags #~(list "-Dxwayland")   ;experimental xwayland support
      #:zig-release-type "safe"
      #:phases
      #~(modify-phases %standard-phases
	  (delete 'validate-runpath)
	  (add-after 'install 'create-desktop-file
	    (lambda* (#:key outputs #:allow-other-keys)
	      (let* ((out (assoc-ref outputs "out"))
		     (wayland-sessions (string-append out "/share/wayland-sessions")))
		(mkdir-p wayland-sessions)
		(call-with-output-file
		    (string-append wayland-sessions "/river.desktop")
		  (lambda (file)
		    (format file
			    "[Desktop Entry]~@
			Name=river~@
			Comment=Dynamic tiling Wayland compositor~@
			Exec=~a/bin/river~@
			TryExec=~@*~a/bin/river~@
			Icon=~@
			Type=Application~%"
			    out))))))
	  )))
    ))



;; FIXME: don't build... error: root struct of file 'std' has no member named 'Build' const Build = std.Build
(define-public k8x1d-river-with-desktop-entry-next
  (package
    (name "k8x1d-river-with-desktop-entry-next")
    (version "0.2.6")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/riverwm/river")
	     (commit (string-append "v" version))
	     (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256
	(base32 "1v0wgl3nf8b0yqg3sjm24vhaf31wp41a33bmiam81i8dj2bzrxi4"))))
    (build-system zig-build-system)
    (arguments
     (list
      #:zig-build-flags #~(list "-Dxwayland")   ;experimental xwayland support
      #:zig-release-type "safe"
      #:phases
      #~(modify-phases %standard-phases
	  (delete 'validate-runpath)
	  (add-after 'install 'create-desktop-file
	    (lambda* (#:key outputs #:allow-other-keys)
	      (let* ((out (assoc-ref outputs "out"))
		     (wayland-sessions (string-append out "/share/wayland-sessions")))
		(mkdir-p wayland-sessions)
		(call-with-output-file
		    (string-append wayland-sessions "/river.desktop")
		  (lambda (file)
		    (format file
			    "[Desktop Entry]~@
			Name=river~@
			Comment=Dynamic tiling Wayland compositor~@
			Exec=~a/bin/river~@
			TryExec=~@*~a/bin/river~@
			Icon=~@
			Type=Application~%"
			    out))))))
	  )))
    (native-inputs (list libevdev
			 libxkbcommon
			 pkg-config
			 pixman
			 scdoc
			 python
			 wayland
			 wayland-protocols
			 wlroots))
    (home-page "https://github.com/riverwm/river")
    (synopsis "Dynamic tiling Wayland compositor")
    (description
     "River is a dynamic tiling Wayland compositor with flexible
runtime configuration.  It can run nested in an X11/Wayland session or also
directly from a tty using KMS/DRM.")
    (license license:gpl3)))

;; from https://github.com/rakino/Rosenthal/blob/trunk/rosenthal/packages/wm.scm
(define-public python-xkbcommon
  (package
    (name "python-xkbcommon")
    (version "0.8")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "xkbcommon" version))
       (sha256
        (base32 "05nmjnvpgcday9bax4ngyzbr05a2kdw88v6zlj3mclmpylxrgrav"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-cffi libxkbcommon))
    (home-page "https://github.com/sde1000/python-xkbcommon")
    (synopsis "Bindings for libxkbcommon using cffi")
    (description "Bindings for libxkbcommon using cffi")
    (license license:expat)))

;; TODO: repair, "No module named 'pywayland.pywayland_scanner'" in sanity-check
(define-public python-pywayland
  (package
    (name "python-pywayland")
    (version "0.4.17")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pywayland" version))
       (sha256
        (base32 "0my64cdcdcl2ra0i8q521hb3qpc9yqz1mwq7mhaplbwccc11kzgp"))))
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
	   (delete 'sanity-check)
      )))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-cffi wayland))
    (native-inputs (list pkg-config python-pytest wayland))
    (home-page "")
    (synopsis
     "Python bindings for the libwayland library written in pure Python")
    (description
     "Python bindings for the libwayland library written in pure Python")
    (license #f)))

;; TODO: repair, fatal error wlr_output.h:12:10: fatal error: pixman.h: No such file or directory
(define-public python-pywlroots
  (package
    (name "python-pywlroots")
    (version "0.16.6")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pywlroots" version))
       (sha256
        (base32 "0hcdsssfihwm8vmbpsqav8nzshm8slp6qc4yg5695lfh0awgadr1"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-cffi python-pywayland python-xkbcommon pixman))
    (native-inputs (list python-pytest wlroots pixman pkg-config))
    (inputs
     (list wlroots))
    (home-page "")
    (synopsis "Python binding to the wlroots library using cffi")
    (description "Python binding to the wlroots library using cffi")
    (license #f)))



(define-public qtile-wayland
  (package
    (inherit qtile)
    (name "qtile-wayland")
    (arguments
     (list
      #:tests? #f
      #:phases
      ;; TODO: find way to construct on modified phases from initial package definition
      #~(modify-phases %standard-phases
	  (add-after 'unpack 'patch-paths
	    (lambda* (#:key inputs #:allow-other-keys)
	      (substitute* "libqtile/pangocffi.py"
		(("^gobject = ffi.dlopen.*")
		 (string-append "gobject = ffi.dlopen(\""
				(assoc-ref inputs "glib") "/lib/libgobject-2.0.so.0\")\n"))
		(("^pango = ffi.dlopen.*")
		 (string-append "pango = ffi.dlopen(\""
				(assoc-ref inputs "pango") "/lib/libpango-1.0.so.0\")\n"))
		(("^pangocairo = ffi.dlopen.*")
		 (string-append "pangocairo = ffi.dlopen(\""
				(assoc-ref inputs "pango") "/lib/libpangocairo-1.0.so.0\")\n")))))
	  (add-after 'install 'install-xsession
	    (lambda* (#:key outputs #:allow-other-keys)
	      (let* ((out (assoc-ref outputs "out"))
		     (xsessions (string-append out "/share/xsessions"))
		     (qtile (string-append out "/bin/qtile start")))
		(mkdir-p xsessions)
		(copy-file "resources/qtile.desktop" (string-append xsessions "/qtile.desktop"))
		(substitute* (string-append xsessions "/qtile.desktop")
		  (("qtile start") qtile)))))
	  (add-after 'install-xsession 'create-desktop-file
	    (lambda* (#:key outputs #:allow-other-keys)
	      (let* ((out (assoc-ref outputs "out"))
		     (wayland-sessions (string-append out "/share/wayland-sessions")))
		(mkdir-p wayland-sessions)
		(call-with-output-file
		    (string-append wayland-sessions "/qtile-wayland.desktop")
		  (lambda (file)
		    (format file
			    "[Desktop Entry]~@
			Name=Qtile (Wayland)~@
			Comment=A full-featured, hackable tiling window manager written and configured in Python~@
			Exec=~a/bin/qtile start -b wayland~@
			TryExec=~@*~a/bin/qtile start -b wayland~@
			Icon=~@
			Type=Application~%"
			    out))))))
	  )))
    (inputs
      (list glib pango pulseaudio wlroots python-pywlroots))
    ))


(define-public k8x1d-velox
  (package
    (inherit velox)
    (name "k8x1d-velox")
    (arguments
     `(#:tests? #f ;no tests
       #:make-flags (list (string-append "CC="
					 ,(cc-for-target))
			  (string-append "PREFIX=" %output))
       #:phases
       (modify-phases %standard-phases
	   (delete 'configure)
	   (add-after 'install 'create-desktop-file
	     (lambda* (#:key outputs #:allow-other-keys)
	       (let* ((output (assoc-ref outputs "out"))
		      (wayland-sessions (string-append output "/share/wayland-sessions")))
		 (mkdir-p wayland-sessions)
		 (call-with-output-file
		     (string-append wayland-sessions "/velox.desktop")
		   (lambda _
		     (format #t
			     "[Desktop Entry]~@
			Name=velox~@
			Comment=velox is a simple window manager based on swc. It is inspired by dwm and xmonad.~@
			Exec=~a/bin/velox~@
			TryExec=~@*~a/bin/velox~@
			Icon=~@
			Type=Application~%"
			     output))
		   ) #t)))
	   )
    ))
    (inputs (list libinput libxkbcommon wayland wld))
    (propagated-inputs (list swc))
    (native-inputs (list pkg-config))
    ))
