(define-module (k8x1d packages julia)
  #:use-module (gnu packages)
  #:use-module (gnu packages julia)
  #:use-module (gnu packages julia-xyz)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system julia)
  #:use-module ((guix licenses) #:prefix license:)
  #:export (%k8x1d-julia))


;; TODO: write
(define-public julia-compose
  (package
    (name "julia-gadfly")
    (version "1.3.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/GiovineItalia/Gadfly.jl")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "0k55rhqx57q1wghq0mgb4dqh8f93z6956s48iq8hvqsic5mmawxv"))))
    (build-system julia-build-system)
    (inputs                             ;required for tests
     (list julia-colors))
    (home-page "https://github.com/GiovineItalia/Gadfly.jl")
    (synopsis "Crafty statistical graphics for Julia.")
    (description "Gadfly is a system for plotting and visualization written in Julia. It is based largely on Hadley Wickhams's ggplot2 for R and Leland Wilkinson's book The Grammar of Graphics. It was Daniel C. Jones' brainchild and is now maintained by the community. Please consider citing it if you use it in your work.")
    (license license:expat)))


(define-public julia-gadfly
  (package
    (name "julia-gadfly")
    (version "1.3.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/GiovineItalia/Gadfly.jl")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "0k55rhqx57q1wghq0mgb4dqh8f93z6956s48iq8hvqsic5mmawxv"))))
    (build-system julia-build-system)
    (inputs                             ;required for tests
     (list julia-colors))
    (home-page "https://github.com/GiovineItalia/Gadfly.jl")
    (synopsis "Crafty statistical graphics for Julia.")
    (description "Gadfly is a system for plotting and visualization written in Julia. It is based largely on Hadley Wickhams's ggplot2 for R and Leland Wilkinson's book The Grammar of Graphics. It was Daniel C. Jones' brainchild and is now maintained by the community. Please consider citing it if you use it in your work.")
    (license license:expat)))

(define-public %k8x1d-julia
  (append
   (specifications->packages
    (list
     "julia" ;; High-performance dynamic language for technical computing
     "julia-csv" ;; Fast and flexible delimited-file reader/writer
     "julia-dataframes" ;; In-memory tabular data
     "julia-http" ;; HTTP support for Julia
     "julia-gumbo" ;; Julia wrapper around Google's gumbo C library for parsing HTML
     "julia-json" ;; JSON parsing and printing library for Julia
     ))
   (list
    ;; julia-gadfly ;; Crafty statistical graphics for Julia.
    )))
