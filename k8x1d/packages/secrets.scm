(define-module (k8x1d packages secrets)
  #:use-module (gnu packages)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages aidc)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages cross-base)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:export (%k8x1d-secrets))

(define-public pinentry-bemenu-newer
  (package
    (inherit pinentry-bemenu) 
    (name "pinentry-bemenu")
    (version "0.13.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/t-8ch/pinentry-bemenu")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0c23fy8ila832hx2gw63bpbzg57jv3dfjdxd4cid8ghjy9xp536p"))))
    ))


(define-public %k8x1d-secrets
  (append
   (specifications->packages
    (list
     "password-store" ;; Encrypted password manager
     "pass-otp" ;; Pass extension for managing one-time-password (OTP) tokens
     "zbar" ;; Bar code reader
     "gnupg" ;; GNU Privacy Guard
     "pinentry" ;; GnuPG's interface to passphrase input
     "pinentry-emacs" ;; GnuPG's interface to passphrase input
     "pinentry-gnome3" ;; GnuPG's interface to passphrase input
     "pinentry-qt" ;; GnuPG's interface to passphrase input
     "gnome-keyring" ;; Daemon to store passwords and encryption keys
     "libsecret" ;; GObject bindings for "Secret Service" API
     ))
  (list
   ;;pinentry-bemenu ;; Pinentry implementation based on `bemenu'
   pinentry-bemenu-newer
   )
  ))
