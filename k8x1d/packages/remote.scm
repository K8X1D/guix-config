(define-module (k8x1d packages remote)
  #:use-module (gnu packages)
  #:use-module (gnu packages vpn)
  #:use-module (gnu packages rdesktop)
  #:use-module (gnu packages vnc)
  #:export (%k8x1d-remote))

(define-public %k8x1d-remote
  (specifications->packages
  (list
   "openconnect" ;; Client for Cisco VPN
   "rdesktop" ;; Client for Windows Terminal Services
   "remmina" ;; Remote Desktop Client
   )))
