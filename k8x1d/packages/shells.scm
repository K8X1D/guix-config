(define-module (k8x1d packages shells)
  #:use-module (gnu packages)
  #:use-module (gnu packages shellutils)
  #:export (%k8x1d-shells))

(define-public %k8x1d-shells
  (specifications->packages
   (list
    "zsh-completions" ;; Additional completion definitions for Zsh 
    "zsh-autosuggestions" ;; Fish-like autosuggestions for zsh
    )))
