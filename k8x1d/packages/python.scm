(define-module (k8x1d packages python)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages check)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages vpn)
  #:use-module (gnu packages webkit)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module ((guix licenses) #:prefix license:)
  #:export (%k8x1d-python))



;; FIXME: doesn't build
(define-public python-bracex
  (package
   (name "python-bracex")
   (version "2.4")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "bracex" version))
     (sha256
      (base32 "1ssb7339p60p3pfzmqc1dahix78jvwzqyylbspz63x9cyhfsyzm2"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-hatchling))
   (arguments `(#:tests? #f))
   (home-page "")
   (synopsis "Bash style brace expander.")
   (description "Bash style brace expander.")
   (license #f))
  )

(define-public python-wcmatch
  (package
   (name "python-wcmatch")
   (version "8.5")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "wcmatch" version))
     (sha256
      (base32 "1czgm211h7lr9skv9j88kjdb6wly5yzz660srcxvyp7ps1r7bhc6"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-bracex))
   (arguments `(#:tests? #f))
   (home-page "")
   (synopsis "Wildcard/glob file name matcher.")
   (description "Wildcard/glob file name matcher.")
   (license #f))
  )

(define-public python-click-option-group
  (package
   (name "python-click-option-group")
   (version "0.5.6")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "click-option-group" version))
     (sha256
      (base32 "0xz73kgavgq8zxrfmlb2fly9l1i55ds4752h718cq61mhw1ngl4p"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-click))
   (native-inputs (list python-coverage python-coveralls python-pytest
			            python-pytest-cov))
   (home-page "https://github.com/click-contrib/click-option-group")
   (synopsis "Option groups missing in Click")
   (description "Option groups missing in Click")
   (license #f))
  )

(define-public python-glom
  (package
   (name "python-glom")
   (version "20.11.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "glom" version))
     (sha256
      (base32 "04pba09vdr3qjvqvy14g60fscdsi35chbbyqpczdp76cpir101al"))))
   (build-system python-build-system)
   (arguments
    '(#:tests? #f
      #:phases
      (modify-phases %standard-phases
                     (replace 'check
                              (lambda* (#:key tests? inputs outputs #:allow-other-keys)
                                (when tests?
                                  ;; Make installed executable available for running the tests.
                                  (setenv "PATH"
                                          (string-append (assoc-ref outputs "out") "/bin"
                                                         ":" (getenv "PATH")))
                                  (invoke "pytest" "-v")))))))
   (native-inputs
    (list python-pytest python-pyyaml))
   (propagated-inputs
    (list python-attrs python-boltons python-face))
   (home-page "https://github.com/mahmoud/glom")
   (synopsis "Declaratively restructure data")
   (description "Real applications have real data, and real data
nests---objects inside of objects inside of lists of objects.  glom is a new
and powerful way to handle real-world data, featuring:

@itemize
@item Path-based access to nested data structures
@item Readable, meaningful error messages
@item Declarative data transformation, using lightweight, Pythonic specifications
@item Built-in data exploration and debugging features
@end itemize")
   (license license:bsd-3)))



(define-public python-semgrep
  (package
   (name "python-semgrep")
   (version "1.44.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "semgrep" version))
     (sha256
      (base32 "0jqsj2ksc39m2hjjbd9dlxj1xd0dvssl42c76caqpm0gyihnj3ks"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-attrs
			                python-boltons
			                python-click
			                python-click-option-group
			                python-colorama
			                python-defusedxml
			                python-glom ;; doesn't build...
			                python-jsonschema
			                python-packaging
			                python-peewee
			                python-lsp-jsonrpc
			                python-requests
			                python-rich
			                python-ruamel.yaml
			                python-tomli
			                python-typing-extensions
			                python-urllib3
			                python-wcmatch))
   ;; (arguments `(#:tests? #f))
   (home-page "https://github.com/returntocorp/semgrep")
   (synopsis
    "Lightweight static analysis for many languages. Find bug variants with patterns that look like source code.")
   (description
    "Lightweight static analysis for many languages.  Find bug variants with patterns
that look like source code.")
   (license #f))
  )

(define-public python-rchitect
  (package
   (name "python-rchitect")
   (version "0.4.2")
   (source (origin
	        (method url-fetch)
	        (uri (pypi-uri "rchitect" version))
	        (sha256
	         (base32
	          "1gmab3kxw0zyy2rzc59hjj89zijc27rpzrdfbvh9n5rna5w57564"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-cffi python-six))
   (native-inputs (list python-pytest python-pytest-cov python-pytest-mock))
   (arguments `(#:tests? #f))
   (home-page "https://github.com/randy3k/rchitect")
   (synopsis "Mapping R API to Python")
   (description "Mapping R API to Python")
   (license #f))
  )


(define-public python-radian
  (package
   (name "python-radian")
   (version "0.6.7")
   (source (origin
	        (method url-fetch)
	        (uri (pypi-uri "radian" version))
	        (sha256
	         (base32
	          "0d2i8dqdjlf3i10q7rhrcflpmrj1xfji8rnh1skm0zhyb33f2zr9"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-prompt-toolkit python-pygments
			                python-rchitect))
   (native-inputs (list python-coverage python-pexpect python-ptyprocess
			            python-pyte python-pytest))
   (arguments `(#:tests? #f))
   (home-page "https://github.com/randy3k/radian")
   (synopsis "A 21 century R console")
   (description "This package provides a 21 century R console")
   (license #f))
  )

(define-public python-userpath
  (package
   (name "python-userpath")
   (version "1.9.1")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "userpath" version))
     (sha256
      (base32 "15i7w1sh60f7i23rqls72s6rdkgw4cxvz08p82v19jcqimr7d0ff"))))
   (build-system pyproject-build-system)
   (arguments `(#:tests? #f))
   (propagated-inputs (list python-click python-hatchling))
   (home-page "")
   (synopsis "Cross-platform tool for adding locations to the user PATH")
   (description "Cross-platform tool for adding locations to the user PATH")
   (license #f)))

(define-public python-pipx
  (package
   (name "python-pipx")
   (version "1.2.1")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "pipx" version))
     (sha256
      (base32 "0ankhqfwaal0wjhcf7grqs223kjfrbcp2snwyhfsik4pbb07g1v9"))))
   (build-system pyproject-build-system)
   (arguments `(#:tests? #f))
   (propagated-inputs (list python-argcomplete python-colorama
                            python-importlib-metadata python-packaging
                            python-userpath))
   (home-page "")
   (synopsis "Install and Run Python Applications in Isolated Environments")
   (description
    "Install and Run Python Applications in Isolated Environments")
   (license #f)))

(define-public python-lsprotocol
  (package
   (name "python-lsprotocol")
   (version "2023.0.0b1")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "lsprotocol" version))
     (sha256
      (base32 "1m0hynwxnc5dcjq342ps84rm83250yc7ilfxfgrkjmmxbijx98pp"))))
   (build-system pyproject-build-system)
   (native-inputs (list python-pytest))
   (propagated-inputs (list python-attrs python-cattrs python-flit-core))
   (home-page "")
   (synopsis "Python implementation of the Language Server Protocol.")
   (description "Python implementation of the Language Server Protocol.")
   (license #f))
  )


(define-public %k8x1d-python
  (append
   (specifications->packages
    (list
     "python" ;; High-level, dynamically-typed programming language
     "python-pandas" ;; Data structures for data analysis, time series, and statistics

     ;; IDE support
     "bpython" ;; Fancy interface to the Python interpreter
     "python-lsp-server" ;; Python implementation of the Language Server Protocol

     ;; LSP Bridge depedencies
     "python-epc" ;; Remote procedure call (RPC) stack for Emacs Lisp and Python
     "python-orjson" ;; Python JSON library supporting dataclasses, datetimes, and numpy
     "python-sexpdata" ;; S-expression parser for Python
     "python-six" ;; Python 2 and 3 compatibility utilities
     "python-paramiko"  ;; SSHv2 protocol library
     "python-beautifulsoup4" ;; Python screen-scraping library
     "python-tomli" ;; Small and fast TOML parser
     "python-pylint" ;; Advanced Python code static checker
     "python-mutagen" ;; Read and write audio tags
     "python-black" ;; The uncompromising code formatter

     ;; Numerical
     "python-numpy" ;; Fundamental package for scientific computing with Python
     "zlib" ;; Compression library

     ;; Notebook
     ;; "jupyter" ;; Web application for interactive documents
     "python-ipykernel" ;; IPython Kernel for Jupyter

     "python-scipy" ;; The Scipy library provides efficient numerical routines

     ))
   (list
    python-radian ;; A 21 century R console
    ;; python-lsprotocol ;; Python implementation of the Language Server Protocol.
    python-pipx ;; Install and Run Python Applications in Isolated Environments
    ;; python-semgrep ;; Lightweight static analysis for many languages
    )))
