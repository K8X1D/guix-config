(define-module (k8x1d packages fonts)
  #:use-module (gnu packages)
  #:export (%k8x1d-fonts))

(define-public %k8x1d-fonts
  (specifications->packages
  (list
   "font-gnu-unifont:bin"
   "font-fira-mono"
   "font-jetbrains-mono"
   "font-fira-code"
   "font-fira-sans"
   "font-iosevka"
   "font-iosevka-aile"
   "font-iosevka-term"
   "font-awesome"
   "font-dejavu"
   "font-hack"
   "fontconfig"
   )))
