(define-module (k8x1d packages virtualization)
  #:use-module (gnu packages)
  #:use-module (gnu packages virtualization)
  #:use-module (gnu packages spice)
  #:export (%k8x1d-virtualization))

(define-public %k8x1d-virtualization
  (specifications->packages
   (list
   ;; "qemu" ;; Machine emulator and virtualizer
   "virt-manager" ;; Manage virtual machines
   "virt-viewer" ;; Graphical console client for virtual machines
   ;; "libvirt" ;; Simple API for virtualization 
   "spice-vdagent" ;; Spice agent for Linux
    )))
