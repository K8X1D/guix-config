(define-module (k8x1d packages building-tools)
  #:use-module (gnu packages)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages guile)
  #:export (%k8x1d-building-tools))



(define-public %k8x1d-building-tools
  (specifications->packages
   (list
    "rust-cargo" ;; Package manager for Rust
    "gcc-toolchain" ;; Complete GCC tool chain for C/C++ development
    "make" ;; Remake files automatically
    "cmake" ;; Cross-platform build system
    "pkg-config" ;; Helper tool used when compiling applications and libraries
    "autoconf" ;; Create source code configuration scripts
    "automake" ;; Making GNU standards-compliant Makefiles
    "texinfo" ;; The GNU documentation format
    "guile" ;; Scheme implementation intended especially for extensions
    )))
