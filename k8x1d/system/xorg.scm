(define-module (k8x1d system xorg)
  :use-module (guix gexp)
  :use-module (rnrs io ports)
  :use-module (guix packages)
  :use-module (guix download)
  :use-module (k8x1d system keyboard)
  :use-module (gnu packages xorg)
  :use-module (gnu services xorg)
  #:export (%frame14-xorg-configuration))

(define xorg-server-newest
  (package
   (inherit xorg-server)
   (version "21.1.8")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://xorg.freedesktop.org/archive/individual"
                                "/xserver/xorg-server-" version ".tar.xz"))
            (sha256
             (base32
              "0lmimvaw9x0ymdhjfqsrbx689bcapy8c24ajw9705j2harrxpaiq"))))))

(define %xorg-monitor (call-with-input-file "Xorg/xorg.conf.d/10-monitor.conf" get-string-all))
;; FIXME: new modesetting driver is not used
(define %xorg-intel (call-with-input-file "Xorg/xorg.conf.d/20-intel.conf" get-string-all))
(define %xorg-touchpad (call-with-input-file "Xorg/xorg.conf.d/70-synaptics.conf" get-string-all))

(define %frame14-xorg-extra-config (append (list
					    %xorg-monitor
					    "\n"
					    %xorg-intel
					    "\n"
					    %xorg-touchpad
					    )))

(define %frame14-xorg-configuration
  (xorg-configuration
   (keyboard-layout %frame14-keyboard-layout)
   (modules %default-xorg-modules)
   (resolutions '((2256 1504)))
   ;;(server xorg-server-newest)
   (extra-config %frame14-xorg-extra-config)))

