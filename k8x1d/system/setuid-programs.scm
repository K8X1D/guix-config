(define-module (k8x1d system setuid-programs)
  :use-module (guix gexp)
  :use-module (gnu system)
  :use-module (gnu system setuid)
  :use-module (gnu packages linux)
  :use-module (gnu packages wm)
  #:export (%frame14-setuid-programs))

(define-public %frame14-setuid-programs
  (append (list
	   (setuid-program (program (file-append light "/bin/light")))
	   (setuid-program (program (file-append wlr-randr "/bin/wlr-randr"))))
	  %setuid-programs))
