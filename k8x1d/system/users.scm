(define-module (k8x1d system users)
  :use-module (gnu system shadow)
  :use-module (guix gexp)
  :use-module (gnu packages shells)
  #:export (%frame14-users
	    %frame14-groups))

(define-public %frame14-users
  (cons*
   (user-account
    (name "k8x1d")
    (comment "Kevin Kaiser")
    (group "users")
    (shell (file-append zsh "/bin/zsh"))
    (home-directory "/home/k8x1d")
    (supplementary-groups '("wheel"   ; allow use of sudo, etc.
			    "audio"   ; sound card
			    "video"   ; video devices such as webcams
			    "input"
			    "lp" ;; bluetooth
			    "netdev" ;; allow modification to network
			    "docker" ;; allow non-root use of docker
			    "adbusers"   ;for adb
			    "libvirt" ;; libvirt
			    "kvm" ;; solve https://www.reddit.com/r/GUIX/comments/118m17b/how_do_i_enable_kvm_to_work_with_qemu_and/
			    ;; "dav_group" ;; davfs2
			    "openvpn"
			    )))
   (user-account
    (name "openvpn")
    (comment "openvpn")
    (group "openvpn")
    )
   %base-user-accounts))


(define-public %frame14-groups
  (cons*
   (user-group
    (name "openvpn"))
   %base-groups))
