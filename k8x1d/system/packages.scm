(define-module (k8x1d system packages)
  #:use-module (dtao-guile packages)
  #:use-module (gnu packages android)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages display-managers)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages kde-plasma)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages kde-systemtools)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages video)
  #:use-module (gnu packages virtualization)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages zig-xyz)
  #:use-module (gnu system)
  #:use-module (k8x1d packages suckless)
  #:use-module (k8x1d packages wm)
  #:use-module (nongnu packages video)
  #:use-module (rosenthal packages wm)
  #:export (%frame14-packages))

(define-public %frame14-packages
  (append
   (list
    
    ;; To remove
    ;; k8x1d-dwl foot bemenu wlr-randr swaylock-effects ;; dwl set-up
   ;;  swayfx waybar alacritty bemenu wlroots
    ;; k8x1d-dwm k8x1d-st k8x1d-slstatus slock bemenu ;; dwm set-up
    k8x1d-dwl-guile-xwayland foot bemenu dtao-guile acpi wlr-randr swaylock-effects ;; dwl set-up
    ;; k8x1d-river-with-desktop-entry foot ;; river
    ;; chili-sddm-theme ;; Chili theme for SDDM
    ;; hyprland kitty dolphin wofi ;; hyprland
    ;; k8x1d-velox ;; velox
    ;; qtile-wayland ;; qtile

    qemu ;; kvm
    git ;; Distributed version control system
    openssh ;; Client and server for the secure shell (ssh) protocol 
    nss-certs ;; CA certificates from Mozilla
    intel-media-driver libva-utils ;; intel support
    v4l-utils ;; Realtime video capture utilities for Linux 
    adb fastboot android-file-transfer ;; Android interaction
    davfs2 ;; Mount remote WebDAV resources in the local file system
    )
   %base-packages)
  )
