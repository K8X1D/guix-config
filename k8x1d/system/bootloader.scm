(define-module (k8x1d system bootloader)
  #:use-module (k8x1d system keyboard)
  #:use-module (gnu packages xorg)
  #:use-module (gnu system file-systems)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:export (%frame14-bootloader-configuration))

(define-public %frame14-bootloader-configuration
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (timeout 2)
   (targets (list "/boot/efi"))
   (theme (grub-theme
	   (inherit (grub-theme))
	   (resolution '(2256 . 1504))))
   ;; FIXME: store-directory-prefix been defined for btrfs, automatic prepend of "/gnu" prevent setting correct path to linux and initrd
   (menu-entries (list
		  (menu-entry
		   (label "Arch Linux")
		   (linux "/boot/efi/vmlinuz-linux")
		   (initrd "/boot/efi/initramfs-linux.img")
		   (linux-arguments '("root=UUID=6ba97646-65c5-4bf6-ac8d-c538bf93d68c" "rw" "quiet" "nowatchdog" "acpi_osi=\"!Windows 2020\"" "mem_sleep_default=deep" "nvme.noacpi=1" "i915.enable_psr=1" "modprobe.blacklist=hid_sensor_hub" "splash"))
		  )))
   (keyboard-layout %frame14-keyboard-layout))
  )
