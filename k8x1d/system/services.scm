(define-module (k8x1d system services)
  #:use-module (gnu packages android)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu services base)
  #:use-module (gnu services cups)
  #:use-module (gnu services databases)
  #:use-module (gnu services dbus)
  #:use-module (gnu services desktop)
  ;; #:use-module (gnu services docker)
  #:use-module (gnu services linux)
  #:use-module (gnu services networking)
  #:use-module (gnu services nix)
  #:use-module (gnu services pm)
  #:use-module (gnu services sddm)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services sound)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services vpn)
  #:use-module (gnu services xorg)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix)
  #:use-module (ice-9 textual-ports)
  #:use-module (k8x1d system xorg)
  #:use-module (k8x1d services docker)
  #:export (%frame14-services))

;; Cpupower
;; Work in progress
(define-record-type* <cpupower-configuration>
  cpupower-configuration make-cpupower-configuration
  cpupower-configuration?
  ;; (epb                         
  ;; (non-negative-integer 7)
  ;; "Intel performance and energy bias hint")
  (cpupower            cpupower-cpupower               ;file-like
		       (default cpupower)))

(define (cpupower-shepherd-service config)
  (list
   (shepherd-service
    (provision '(cpupower))
    (documentation "Run cpupower, a Linux kernel tool to examine and tune power saving related features of your processor.")
    ;; (start #~(make-forkexec-constructor
    ;; (list #$(file-append cpupower "/bin/cpupower") "-b" "epb" "15")
    ;; #:log-file "/var/log/cpupower.log"))
    (start #~(make-forkexec-constructor
              '(#$(file-append (cpupower-cpupower config) "/bin/cpupower")
                  ;; "set" "--perf-bias" "15")))
		  "--cpu" "4,5,6,7,8,9,10,11,12,13,14,15" "set" "--perf-bias" "15")))
    (stop #~(make-kill-destructor))
    (one-shot? #t)
    )))

(define cpupower-service-type
  (service-type
   (name 'cpupower)
   (extensions (list (service-extension shepherd-root-service-type
					cpupower-shepherd-service)))
   (description "Run cpupower, a Linux kernel tool to examine and tune power saving related features of your processor.")
   (default-value (cpupower-configuration))
   ))

;; Polkit for network-manager
;; from https://www.reddit.com/r/GUIX/comments/ki7tfo/adding_polkit_rules_the_guix_way/
(define my-polkit-wheel
  (file-union
   "my-polkit-wheel"
   `(("share/polkit-1/rules.d/00-my-wheel.rules"
      ,(plain-file
        "00-my-wheel.rules"
        "polkit.addRule(function(action, subject) {
    if (subject.isInGroup(\"wheel\")) {
        return polkit.Result.YES
    }
});
")))))


;; Udev rules
(define %backlight-udev-rule
  (udev-rule
   "90-backlight.rules"
   (call-with-input-file "rules.d/90-backlight.rules" get-string-all)
   ))
(define %usb-power-save-udev-rule
  (udev-rule
   "50-usb_power_save.rules"
   (call-with-input-file "rules.d/50-usb_power_save.rules" get-string-all)
   ))
(define %bluetooth-turn-off-udev-rule
  (udev-rule
   "50-bluetooth.rules"
   (call-with-input-file "rules.d/50-bluetooth.rules" get-string-all)
   ))
(define %wifi-powersave-udev-rule
  (udev-rule
   "70-wifi-powersave.rules"
   (call-with-input-file "rules.d/70-wifi-powersave.rules" get-string-all)
   ))
(define %initial-backlight-udev-rule
  (udev-rule
   "81-backlight.rules"
   (call-with-input-file "rules.d/81-backlight.rules" get-string-all)
   ))

(define %udev-services
  (list
   (udev-rules-service 'backlight-control %backlight-udev-rule)
   ;; (udev-rules-service 'backlight-init %initial-backlight-udev-rule)
   (udev-rules-service 'usb-power-off %usb-power-save-udev-rule)
   (udev-rules-service 'wifi-powersave %wifi-powersave-udev-rule)
   (udev-rules-service 'android android-udev-rules
		       #:groups '("adbusers"))

   ))


(define %powermanagement-services
  (list
   ;; Power management
   (service tlp-service-type
	    (tlp-configuration
	     (tlp-enable? #t)
	     (tlp-default-mode "BAT")
	     (nmi-watchdog? #f)
	     (usb-autosuspend? #t) ;; doesn't seems to work... udev rule created

	     ;; Batt config
	     (disk-idle-secs-on-bat 2)
	     (max-lost-work-secs-on-bat 60)
	     (cpu-scaling-governor-on-bat '("powersave"))
	     (cpu-min-perf-on-bat 0)
	     (cpu-max-perf-on-bat 50)
	     (cpu-boost-on-bat? #f)
	     (sched-powersave-on-bat? #t)
	     (energy-perf-policy-on-bat "powersave")
	     (pcie-aspm-on-bat "powersupersave")

	     ;; AC config
	     (disk-idle-secs-on-ac 2)
	     (max-lost-work-secs-on-ac 60)
	     (cpu-scaling-governor-on-ac '("powersave")) ;; test for temperature manaagment
	     (cpu-min-perf-on-ac 0)
	     (cpu-max-perf-on-ac 100)
	     (cpu-boost-on-ac? #t)
	     (sched-powersave-on-ac? #t)
	     (energy-perf-policy-on-ac "normal")
	     (pcie-aspm-on-ac "default")
	     
	     ))
   (service thermald-service-type) 
   ))


(define %packages-management-services
  (list
   (service guix-service-type
	    (guix-configuration
	     (substitute-urls
	      (append
	       (list
		;; "https://bordeaux-us-east-mirror.cbaines.net" ;; mirrors of bordeaux.guix.gnu.org in USA ;; often too slow
		"https://substitutes.nonguix.org" ;; nonguix
		;; Don't seems to work anymore
		;; see https://github.com/guix-science/guix-science/issues/22
		;; "https://substitutes.guix.psychnotebook.org"
		"https://guix.bordeaux.inria.fr" ;; guix science 
		)
	       %default-substitute-urls))
	     (authorized-keys
	      (append
	       ;; TODO: keys elsewhere in project
	       ;; List of extra channels: 
	       ;; https://hpc.guix.info/channels/
	       (list
		(local-file "nonguix-key.pub")
		;; Don't seems to work anymore
		;; see https://github.com/guix-science/guix-science/issues/22
		;; (local-file "guix-science-key.pub")
		(local-file "guix-hpc-key.pub") ;; guix-science, guix-cran, guix-past, guix-hpc
		)
	       %default-authorized-guix-keys))
	     ))
   (service nix-service-type)
   ))


(define %virtualization-services
  (list
   (service libvirt-service-type
	    (libvirt-configuration
	     (unix-sock-group "libvirt")
	     (tls-port "16555")))
   (service virtlog-service-type
	    (virtlog-configuration
	     (max-clients 1000)))
   (service qemu-binfmt-service-type
	    (qemu-binfmt-configuration
	     (platforms (lookup-qemu-platforms "arm" "aarch64"))))
   ;; (service qemu-guest-agent-service-type)
   (service docker-service-type)
   ))

(define %lockscreen-services
  (list
   (service screen-locker-service-type
	    (screen-locker-configuration
	     (name "slock")
	     (program (file-append slock "/bin/slock"))))
   (service screen-locker-service-type
	    (screen-locker-configuration
	     (name "i3lock")
	     (program (file-append i3lock-color "/bin/i3lock"))))
   (service screen-locker-service-type
	    (screen-locker-configuration
	     (name "swaylock")
	     (program (file-append swaylock-effects "/bin/swaylock"))
	     (using-pam? #t)
	     (using-setuid? #f)))
   (service screen-locker-service-type
	    (screen-locker-configuration
	     (name "xlock")
	     (program (file-append xlockmore "/bin/xlock"))))
   ))

(define %linux-modules-services
  (list
   (service kernel-module-loader-service-type
	    '("v4l2loopback" ;; obs virtual camera support
	      "snd_aloop" ;; droidcam sound 
	      "vhost_net" ;; better network performance for vms 
	      ))
   ))


(define-public %frame14-services
  (append
   (modify-services %desktop-services
		    (delete gdm-service-type) ;; prefer none or sddm when plasma destop
		    (delete guix-service-type)
		    (delete network-manager-service-type)
		    (delete pulseaudio-service-type)
		    )
   %udev-services
   %powermanagement-services
   %packages-management-services
   %virtualization-services
   %linux-modules-services
   %lockscreen-services
   (list
    (simple-service 'my-polkit-wheel polkit-service-type (list my-polkit-wheel))
    (service gnome-keyring-service-type
	     (gnome-keyring-configuration
	      (pam-services '(("slim" . login)
			      ("su" . login)
			      ("gdm-password" . login)
			      ("login" . login)
			      ("passwd" . login)
			      ("slim" . passwd)
			      ("su" . passwd)
			      ("gdm-password" . passwd)
			      ("login" . passwd)
			      ("passwd" . passwd)))
	      )
	     )

    (service network-manager-service-type
	     (network-manager-configuration
	      (vpn-plugins (list network-manager-openvpn
				 network-manager-openconnect))
	      ))
    (service cups-service-type
	     (cups-configuration
	      (web-interface? #t)
	      (extensions
	       (list cups-filters epson-inkjet-printer-escpr hplip-minimal))))
    (service bluetooth-service-type
	     (bluetooth-configuration
	      (experimental #t)))
    ;; TODO: create db service and add mariadb
    ;; (service postgresql-service-type
    ;; 	     (postgresql-configuration
    ;; 	      (postgresql postgresql-15)
    ;; 	      (data-directory "/extension/Data/postgres/data")
    ;; 	      ))
    ;; (service postgresql-role-service-type
    ;; 	     (postgresql-role-configuration
    ;; 	      (roles
    ;; 	       (list (postgresql-role
    ;; 		      (name "k8x1d")
    ;; 		      (permissions '(createdb login superuser))
    ;; 		      (create-database? #t)))
    ;; 	       )))
    ;; ;; 		     ;; Evocult project
    ;; 		     (postgresql-role
    ;; 		      (name "lamarck")
    ;; 		      (permissions '(login))
    ;; 		      (create-database? #f))
    ;; 		     ))))
    ;; In test
    (service cpupower-service-type)
    )
   ))
