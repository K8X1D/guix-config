(define-module (k8x1d system keyboard)
  :use-module (gnu system keyboard)
  #:export (%frame14-keyboard-layout))

(define-public %frame14-keyboard-layout
  (keyboard-layout "ca" "fr"
                   #:options '("ctrl:nocaps" "altwin:menu_win"))
  )
