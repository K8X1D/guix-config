(define-module (k8x1d system file-systems)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:export (%frame14-file-systems
	    %frame14-swap-spaces
  	    %frame14-mapped-devices))

;; TODO: explore solutions remove obligation to enter decryption pass twice 
;; see https://issues.guix.gnu.org/68524
(define-public %frame14-mapped-devices
   (list (mapped-device
          (source (uuid "8f1d1602-7277-4427-ab73-82f8b1cd034d"))
          (target "guix")
          (type luks-device-mapping)))
)


(define-public %frame14-file-systems
  (cons* 
   (file-system
    (mount-point "/boot/efi")
    (device (uuid "5AF0-8AE7"
		  'fat32))
    (type "vfat"))
   
   (file-system
    (mount-point "/extension")
    (create-mount-point? #t)
    (device (uuid
	     "db47345d-e655-4ecf-acc1-e003bffc35c1"
	     'ext4))
    (type "ext4"))
   
   ;; Mapped
   (file-system
    (device "/dev/mapper/guix")
    (mount-point "/var/log")
    (type "btrfs")
    (options "subvol=log")
    ;; (dependencies mapped-devices))
    (dependencies %frame14-mapped-devices))

   
   (file-system
    (device "/dev/mapper/guix")
    (mount-point "/data")
    (type "btrfs")
    (options "subvol=data")
    ;; (dependencies mapped-devices))
    (dependencies %frame14-mapped-devices))

   (file-system
    (device "/dev/mapper/guix")
    (mount-point "/gnu")
    (type "btrfs")
    (options "subvol=gnu")
    ;; (dependencies mapped-devices))
    (dependencies %frame14-mapped-devices))

   (file-system
    (device "/dev/mapper/guix")
    (mount-point "/")
    (type "btrfs")
    (options "subvol=root")
    ;; (dependencies mapped-devices))
    (dependencies %frame14-mapped-devices))

   (file-system
    (device "/dev/mapper/guix")
    (mount-point "/home")
    (type "btrfs")
    (options "subvol=home")
    ;; (dependencies mapped-devices))
    (dependencies %frame14-mapped-devices))

   %base-file-systems)
  )

(define-public %frame14-swap-spaces
  (list (swap-space
	 (target (uuid
		  "7f10b527-b7d0-4569-88dd-4a223d84f34e"))))
  )

