(use-modules (gnu)
	     (gnu packages linux)
	     (nongnu packages linux)
	     (nongnu system linux-initrd)
	     (k8x1d system bootloader)
	     (k8x1d system file-systems)
	     (k8x1d system services)
	     (k8x1d system xorg)
	     (k8x1d system keyboard)
	     (k8x1d system users)
	     (k8x1d system packages)
	     (k8x1d system setuid-programs))

(operating-system
 (kernel linux-6.6)
 (kernel-loadable-modules (list v4l2loopback-linux-module)) ;; obs virtual camera support
 (kernel-arguments
  ;; https://community.frame.work/t/tracking-linux-battery-life-tuning/6665
  (cons* "mem_sleep_default=deep" "nvme.noacpi=1" ;; more efficient sleep
	 "i915.enable_psr=1" ;; reduce flickering and power consumption
	 ;; see https://ivanvojtko.blogspot.com/2016/04/how-to-get-longer-battery-life-on-linux.html
	 "i915.i915_enable_rc6=1" ;; enables deep, low power state of Intel graphics
	 "i915.i915_enable_fbc=1" ;; Framebuffer compression
	 "i915.lvds_downclock=1" ;; downclock refresh rate of LVDS
	 "modprobe.blacklist=hid_sensor_hub" ;; enable manual changing brightness
	 "quiet" ;; Disable most log messages
	 "splash" ;; show splash screen
	 "nmi_watchdog=0" ;; disable watchdog
	 ;; "rfkill.default_state=0" ;; disable card at boot
	 "acpi_osi=\"!Windows 2020\"" ;; fix regression in s2idle, see https://wiki.archlinux.org/title/Framework_Laptop_13
	 ;; "resume=UUID=7f10b527-b7d0-4569-88dd-4a223d84f34e" ;; swap location for hibernate 
	 "resume=/dev/nvme0n1p2" ;; swap location for hibernate 
	 %default-kernel-arguments))
 (initrd microcode-initrd)
 (firmware (list linux-firmware))
 (locale "en_CA.utf8")
 (timezone "Europe/Brussels")
 ;; (timezone "America/Chicago")
 ;; (timezone "America/New_York")
 (keyboard-layout %frame14-keyboard-layout)
 (host-name "frame14")
 (users %frame14-users)
 (groups %frame14-groups)
 (packages %frame14-packages)
 (setuid-programs %frame14-setuid-programs)
 (services %frame14-services)
 (bootloader %frame14-bootloader-configuration)
 (swap-devices %frame14-swap-spaces)
 (mapped-devices %frame14-mapped-devices)
 (file-systems %frame14-file-systems)
 )
