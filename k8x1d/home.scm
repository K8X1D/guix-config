(use-modules (gnu home)
	     (gnu home services)
	     (k8x1d home packages)
	     (k8x1d home services))

(home-environment
 (packages %k8x1d-packages)
 (services %k8x1d-services)
 )
