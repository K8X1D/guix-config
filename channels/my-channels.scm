(list
 (channel
  (name 'rosenthal)
  (url "https://codeberg.org/hako/rosenthal.git")
  (branch "trunk")
  (introduction
   (make-channel-introduction
    "7677db76330121a901604dfbad19077893865f35"
    (openpgp-fingerprint
     "13E7 6CD6 E649 C28C 3385  4DF5 5E5A A665 6149 17F7"))))
 (channel
  (name 'guix)
  (url "https://git.savannah.gnu.org/git/guix.git")
  (branch "master")
  (introduction
   (make-channel-introduction
    "9edb3f66fd807b096b48283debdcddccfea34bad"
    (openpgp-fingerprint
     "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
 (channel
  (name 'nonguix)
  (url "https://gitlab.com/nonguix/nonguix")
  (branch "master")
  (introduction
   (make-channel-introduction
    "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
    (openpgp-fingerprint
     "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
 (channel
  (name 'guix-science)
  (url "https://github.com/guix-science/guix-science.git")
  (branch "master")
  (introduction
   (make-channel-introduction
    "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
    (openpgp-fingerprint
     "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))
 ;; (channel
 ;;  (name 'guix-hpc)
 ;;  (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
 ;; (branch "master"))
 (channel
  (name 'guix-cran)
  (url "https://github.com/guix-science/guix-cran.git")
  (branch "master"))
 ;; (channel
 ;;  (name 'guix-past)
 ;;  (url "https://gitlab.inria.fr/guix-hpc/guix-past")
 ;;  (branch "master")
 ;;  (introduction
 ;;   (make-channel-introduction
 ;;    "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
 ;;    (openpgp-fingerprint
 ;;     "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
 (channel
  (name 'home-service-dwl-guile)
  (url "https://github.com/engstrand-config/home-service-dwl-guile")
  (branch "main")
  (introduction
   (make-channel-introduction
    "314453a87634d67e914cfdf51d357638902dd9fe"
    (openpgp-fingerprint
     "C9BE B8A0 4458 FDDF 1268  1B39 029D 8EB7 7E18 D68C"))))
 (channel
  (name 'home-service-dtao-guile)
  (url "https://github.com/engstrand-config/home-service-dtao-guile")
  (branch "main")
  (introduction
   (make-channel-introduction
    "64d0b70c547095ddc840dd07424b9a46ccc2e64e"
    (openpgp-fingerprint
     "C9BE B8A0 4458 FDDF 1268  1B39 029D 8EB7 7E18 D68C")))))
