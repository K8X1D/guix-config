(list (channel
        (name 'rosenthal)
        (url "https://codeberg.org/hako/rosenthal.git")
        (branch "trunk")
        (commit
          "0e31205d7392c450bf33de24d7f0cc38aa82d4d5")
        (introduction
          (make-channel-introduction
            "7677db76330121a901604dfbad19077893865f35"
            (openpgp-fingerprint
              "13E7 6CD6 E649 C28C 3385  4DF5 5E5A A665 6149 17F7"))))
      (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "b0b988c41c9e0e591274495a1b2d6f27fcdae15a")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
      (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        (branch "master")
        (commit
          "25bcda2b9107b948a1c858e41aba1b7f95b76228")
        (introduction
          (make-channel-introduction
            "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
            (openpgp-fingerprint
              "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
      (channel
        (name 'guix-science)
        (url "https://github.com/guix-science/guix-science.git")
        (branch "master")
        (commit
          "26fba1a759f67f2c6c22049994db328550d45e30")
        (introduction
          (make-channel-introduction
            "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
            (openpgp-fingerprint
              "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))
      (channel
        (name 'guix-cran)
        (url "https://github.com/guix-science/guix-cran.git")
        (branch "master")
        (commit
          "239348dd513fb73952dcba2b65726651a02090da"))
      (channel
        (name 'home-service-dwl-guile)
        (url "https://github.com/engstrand-config/home-service-dwl-guile")
        (branch "main")
        (commit
          "09045a658a17a914d1bda4d23c04d99c40a1d63b")
        (introduction
          (make-channel-introduction
            "314453a87634d67e914cfdf51d357638902dd9fe"
            (openpgp-fingerprint
              "C9BE B8A0 4458 FDDF 1268  1B39 029D 8EB7 7E18 D68C"))))
      (channel
        (name 'home-service-dtao-guile)
        (url "https://github.com/engstrand-config/home-service-dtao-guile")
        (branch "main")
        (commit
          "ad32455058d588452519edf49f761eb298c9e9bd")
        (introduction
          (make-channel-introduction
            "64d0b70c547095ddc840dd07424b9a46ccc2e64e"
            (openpgp-fingerprint
              "C9BE B8A0 4458 FDDF 1268  1B39 029D 8EB7 7E18 D68C")))))
