(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "d987b75618a62c95c030e7ca53e0972e700c4f06")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
      (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        (branch "master")
        (commit
          "16e9c0d69b7e786f3c86ab5f6f7a20e1f28603ce")
        (introduction
          (make-channel-introduction
            "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
            (openpgp-fingerprint
              "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
      (channel
        (name 'guix-science)
        (url "https://github.com/guix-science/guix-science.git")
        (branch "master")
        (commit
          "25ce86cd74f38d7c17d1d90a8271758959702f43")
        (introduction
          (make-channel-introduction
            "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
            (openpgp-fingerprint
              "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))
      (channel
        (name 'guix-hpc)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc")
        (branch "master")
        (commit
          "05e0bf9b7e2d12bae98953de75292796bebc6a1b"))
      (channel
        (name 'guix-cran)
        (url "https://github.com/guix-science/guix-cran")
        (branch "master")
        (commit
          "a46c15758535b5931ba728d5a22df74a10835b62"))
      (channel
        (name 'guix-past)
        (url "https://gitlab.inria.fr/guix-hpc/guix-past")
        (branch "master")
        (commit
          "1e25b23faa6b1716deaf7e1782becb5da6855942")
        (introduction
          (make-channel-introduction
            "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
            (openpgp-fingerprint
              "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
      (channel
        (name 'home-service-dwl-guile)
        (url "https://github.com/engstrand-config/home-service-dwl-guile")
        (branch "main")
        (commit
          "09045a658a17a914d1bda4d23c04d99c40a1d63b")
        (introduction
          (make-channel-introduction
            "314453a87634d67e914cfdf51d357638902dd9fe"
            (openpgp-fingerprint
              "C9BE B8A0 4458 FDDF 1268  1B39 029D 8EB7 7E18 D68C"))))
      (channel
        (name 'home-service-dtao-guile)
        (url "https://github.com/engstrand-config/home-service-dtao-guile")
        (branch "main")
        (commit
          "ad32455058d588452519edf49f761eb298c9e9bd")
        (introduction
          (make-channel-introduction
            "64d0b70c547095ddc840dd07424b9a46ccc2e64e"
            (openpgp-fingerprint
              "C9BE B8A0 4458 FDDF 1268  1B39 029D 8EB7 7E18 D68C")))))
