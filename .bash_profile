# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

#
# Guix
#

# set locales
export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"

# # Add extra-profile
# # from https://guix.gnu.org/cookbook/en/html_node/Basic-setup-with-manifests.html
# export GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
# # subset
# for i in "$GUIX_EXTRA_PROFILES"/funny; do
#   profile=$i/$(basename "$i")
#   if [ -f "$profile"/etc/profile ]; then
#     GUIX_PROFILE="$profile"
#     . "$GUIX_PROFILE"/etc/profile
#   fi
#   unset profile
# done

# Set default user profile for guix
GUIX_PROFILE="$HOME/.guix-profile"
. "$GUIX_PROFILE/etc/profile"

# Set compiler
export CC=gcc

# Wayland compatibility
if [ "$XDG_SESSION_TYPE" == "wayland" ]; then
    export MOZ_ENABLE_WAYLAND=1
fi

#
# Nix
#
#export FONTCONFIG_FILE=${pkgs.fontconfig.out}/etc/fonts/fonts.conf
export FONTCONFIG_PATH=$HOME/.guix-profile/etc/fonts
export FONTCONFIG_PATH=$HOME/.guix-home/profile/etc/fonts:$FONTCONFIG_PATH
export NIXPKGS_ALLOW_UNFREE=1
XDG_DATA_DIRS="$HOME/.nix-profile/share:$XDG_DATA_DIRS"

# set PATH so it includes nix bin if it exists
if [ -d "$HOME/.nix-profile/bin" ] ; then
    PATH="$HOME/.nix-profile/bin:$PATH"
fi

PATH=$HOME/.nix-profile/bin:$PATH

# Rstudio
# temp fix for white empty panel
#RSTUDIO_CHROMIUM_ARGUMENTS="--no-sandbox"

# Flatpak ####
XDG_DATA_DIRS="$HOME/.local/share/flatpak/exports/share:$XDG_DATA_DIRS"
alias zoom="flatpak run us.zoom.Zoom"
alias nextcloud="flatpak run com.nextcloud.desktopclient.nextcloud"

#
# Doom emacs
#
#export EMACS=~/.guix-profile/bin/emacs-28.2
#export EMACSLOADPATH=~/.guix-profile/share/emacs/site-lisp
#PATH="$HOME/.config/emacs/bin:$PATH"


#
# Nyxt
#
# Webkit work around
export WEBKIT_DISABLE_SANDBOX_THIS_IS_DANGEROUS=1

#
# Julia
#
#export JULIA_DEPOT_PATH="$HOME/.julia"
# set initial number of threads
export JULIA_NUM_THREADS=6


#
# User dirs
#
export XDG_SCREENSHOTS_DIR=$HOME/Pictures/screenshots



#
# Themes
#
# Managing qt themes through qct5ct
# export QT_QPA_PLATFORMTHEME=qt5ct


#
# ltex
#
#export JAVA_HOME=$HOME/.guix-home/profile 

# direnv set-up
eval "$(direnv hook bash)"


