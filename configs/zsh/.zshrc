# TODO: Transfert env definitions to .zshenv
# TODO: Clean-up
# TODO: accord with .zprofile, see https://wiki.archlinux.org/title/Zsh#Startup/Shutdown_files
######################
# User configuration #
######################

HISTFILE=/home/k8x1d/.config/zsh/.zhistory
HISTSIZE=10000
SAVEHIST=10000
setopt autocd extendedglob nomatch notify
unsetopt beep
bindkey -e

# create a zkbd compatible hash;
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete

# Adjust control key
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# Set application mode
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi


# Completion support
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
zstyle ':completion::complete:*' gain-privileges 1

# History search
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

# autoload -U history-search-end
# zle -N history-beginning-search-backward-end history-search-end
# zle -N history-beginning-search-forward-end history-search-end
# bindkey "^[[A" history-beginning-search-backward-end
# bindkey "^[[B" history-beginning-search-forward-end


# Git support
autoload -Uz vcs_info
#zstyle ':vcs_info:*' enable git svn
#zstyle ':vcs_info:git*' actionformats "%s%b %m%u%c "
zstyle ':vcs_info:git*' formats "%b %m%u%c" 
precmd() {
    vcs_info
}
setopt prompt_subst
# PROMPT="%1~ %F{red}${vcs_info_msg_0_}%f %#"$'\n'" "
PROMPT='%F{cyan}%n@%m%f %F{yellow}%~%f %F{red}${vcs_info_msg_0_}%f $ '$'\n'""

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"



# Nix compatiblity 
export FONTCONFIG_PATH=$HOME/.guix-profile/etc/fonts
export FONTCONFIG_PATH=$HOME/.guix-home/profile/etc/fonts:$FONTCONFIG_PATH
export NIXPKGS_ALLOW_UNFREE=1
XDG_DATA_DIRS="$HOME/.nix-profile/share:$XDG_DATA_DIRS"

# Flatpak compatibility (removed)
# XDG_DATA_DIRS="$HOME/.local/share/flatpak/exports/share:$XDG_DATA_DIRS"
# alias zoom="flatpak run us.zoom.Zoom"
# alias nextcloud="flatpak run com.nextcloud.desktopclient.nextcloud"

# User directory
export XDG_SCREENSHOTS_DIR=$HOME/Pictures/screenshots

# Guix compatibily
if [ -n "$GUIX_ENVIRONMENT" ]
then
    #export PS1="\u@\h \w [env]\$ "
    #export PS1="%{$reset_color%}%B%F{blue}(guix env)%F%f%b"
    export PS1="$PS1%B%F{blue}(guix env)%F%f%b "
fi

# # Add extra-profile
# # Set default user profile for guix
# GUIX_PROFILE="$HOME/.guix-profile"
# . "$GUIX_PROFILE/etc/profile"
# # from https://guix.gnu.org/cookbook/en/html_node/Basic-setup-with-manifests.html
# export GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
# # all
# for i in $GUIX_EXTRA_PROFILES/*; do
#   profile=$i/$(basename "$i")
#   if [ -f "$profile"/etc/profile ]; then
#     GUIX_PROFILE="$profile"
#     . "$GUIX_PROFILE"/etc/profile
#   fi
#   unset profile
# done

# subset (don't work, stop at browser
# for i in "$GUIX_EXTRA_PROFILES"/R "$GUIX_EXTRA_PROFILES"/julia "$GUIX_EXTRA_PROFILES"/latex "$GUIX_EXTRA_PROFILES"/python "$GUIX_EXTRA_PROFILES"/browsers "GUIX_EXTRA_PROFILES"/multimedia "GUIX_EXTRA_PROFILES"/xorg "GUIX_EXTRA_PROFILES"/desktop "GUIX_EXTRA_PROFILES"/wayland; do
#     profile=$i/$(basename "$i")
#     if [ -f "$profile"/etc/profile ]; then
#         GUIX_PROFILE="$profile"
#         . "$GUIX_PROFILE"/etc/profile
#     fi
#     unset profile
# done

# export GUIX_PROFILE


# Emacs
# Integration with eat
# FIXME: result in  tail: cannot open '' for reading: No such file or directory
# [ -n "$EAT_SHELL_INTEGRATION_DIR" ] && \
#   source "$EAT_SHELL_INTEGRATION_DIR/zsh"

# Integration with vterm
vterm_printf() {
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ]); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}
# Directory tracking
vterm_prompt_end() {
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
}
setopt PROMPT_SUBST
PROMPT=$PROMPT'%{$(vterm_prompt_end)%}'

# Integration with eat terminal

[ -n "$EAT_SHELL_INTEGRATION_DIR" ] && \
  source "$EAT_SHELL_INTEGRATION_DIR/zsh"

# Editor
EDITOR=emacs-guix-client

# Java
JAVA_HOME=$HOME/.guix-home/profile

# QT theme manager
QT_QPA_PLATFORMTHEME=qt5ct


# Theme
export GTK_THEME=Breeze-Dark

# Wayland compatibility
export MOZ_ENABLE_WAYLAND=1
export GDK_BACKEND=wayland,x11
export MOZ_ENABLE_WAYLAND=1
export CLUTTER_BACKEND=wayland
export QT_QPA_PLATFORM="wayland:xcb:wayland-egl"
export ECORE_EVAS_ENGINE=wayland-egl
# Sway compatibility with gnome-keyring
export SSH_AUTH_SOCK=$SSH_AUTH_SOCK:/run/user/1000/keyring/ssh

# Export variables
export PROMPT 
export EDITOR
export JAVA_HOME
export QT_QPA_PLATFORMTHEME



# Import plugins
source /home/k8x1d/.guix-home/profile/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# source $HOME/.guix-profile/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# Plugins configurations
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE=fg=5

# Micromamba support
# >>> mamba initialize >>>
export MAMBA_EXE="/gnu/store/7bp76445rxc8yvm02sxq6snjqmk7vf7v-micromamba-bin-1.4.4-0/bin/micromamba";
export MAMBA_ROOT_PREFIX="/home/k8x1d/.micromamba";
__mamba_setup="$("$MAMBA_EXE" shell hook --shell zsh --prefix "$MAMBA_ROOT_PREFIX" 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__mamba_setup"
else
    if [ -f "/home/k8x1d/.micromamba/etc/profile.d/micromamba.sh" ]; then
        . "/home/k8x1d/.micromamba/etc/profile.d/micromamba.sh"
    else
        export  PATH="/home/k8x1d/.micromamba/bin:$PATH"  # extra space after export prevents interference from conda init
    fi
fi
unset __mamba_setup
# <<< mamba initialize <<<


