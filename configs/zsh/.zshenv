# $PATH
typeset -U path PATH

# User configuration
path=($HOME/.local/bin $path) 

# Nix configurations
path=($HOME/.nix-profile/bin $path) # Nix programs
export FONTCONFIG_PATH=$HOME/.guix-profile/etc/fonts
export FONTCONFIG_PATH=$HOME/.guix-home/profile/etc/fonts:$FONTCONFIG_PATH
export NIXPKGS_ALLOW_UNFREE=1
export XDG_DATA_DIRS="$HOME/.nix-profile/share:$XDG_DATA_DIRS"
export NIXOS_OZONE_WL="1" # wayland native support for electron apps 

# Wayland compatibility
export MOZ_ENABLE_WAYLAND=1
export GDK_BACKEND=wayland,x11
export MOZ_ENABLE_WAYLAND=1
export CLUTTER_BACKEND=wayland
export QT_QPA_PLATFORM=wayland:xcb:wayland-egl
export ECORE_EVAS_ENGINE=wayland-egl

# Themes
export QT_QPA_PLATFORMTHEME=qt5ct
export GTK_THEME=Breeze-Dark

# Guile lsp support
# export GUILE_LOAD_PATH=...:${HOME}/.guix-profile/share/guile/site/3.0:$GUILE_LOAD_PATH
# export GUILE_LOAD_COMPILED_PATH=...:${HOME}/.guix-profile/lib/guile/3.0/site-ccache:$GUILE_LOAD_COMPILED_PATH
export GUILE_LOAD_PATH=$HOME/.guix-profile/share/guile/site/3.0:$GUILE_LOAD_PATH
export GUILE_LOAD_COMPILED_PATH=$HOME/.guix-profile/lib/guile/3.0/site-ccache:$GUILE_LOAD_COMPILED_PATH

# gpg-agent has OpenSSH agent emulation
export SSH_AGENT_PID=""
export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh"

# Export
export PATH



