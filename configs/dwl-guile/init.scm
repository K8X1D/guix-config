(setq inhibit-defaults? #t)
(setq tap-to-click? #t)
(setq repeat-rate 50)
(setq repeat-delay 300)
(setq gaps-ih 5)
(setq gaps-iv 5)
(setq border-px 2)
(dwl:set-tty-keys "C-M")
(dwl:set-tag-keys "s" "s-S")
(setq focus-color "#c3e88d")
(setq border-color "#82aaff")
(set-keys
  "s-d"
  '(dwl:spawn
     "bemenu-run"
     "--fn"
     "Iosevka Aile 14"
     "--nf"
     "#82aaff"
     "--nb"
     "#292D3ECC"
     "--af"
     "#82aaff"
     "--ab"
     "#292D3ECC"
     "--tf"
     "#292D3E"
     "--tb"
     "#c3e88d"
     "--hf"
     "#ffcb6b"
     "--hb"
     "#292D3ECC"
     "--ff"
     "#c3e88d"
     "--fb"
     "#292D3E"
     "--bottom"
     "--list"
     "10"
     "--prompt"
     "Launch: "
     "--prefix"
     "          >"
     "--counter"
     "always"
     "--no-spacing"
     "--no-cursor"
     "--ignorecase")
  "s-e"
  '(dwl:spawn "/home/k8x1d/.local/bin/k8x1d-emacs" "-c")
  "s-b"
  '(dwl:spawn "dtao-guile_toggle")
  "s-j"
  '(dwl:focus-stack 1)
  "s-k"
  '(dwl:focus-stack -1)
  "s-l"
  '(dwl:change-master-factor 0.05)
  "s-h"
  '(dwl:change-master-factor -0.05)
  "s-<page-up>"
  '(dwl:change-masters 1)
  "s-<page-down>"
  '(dwl:change-masters -1)
  "s-t"
  '(dwl:cycle-layout 1)
  "s-<left>"
  '(dwl:focus-monitor 'DIRECTION-LEFT)
  "s-<right>"
  '(dwl:focus-monitor 'DIRECTION-RIGHT)
  "s-<up>"
  '(dwl:focus-monitor 'DIRECTION-UP)
  "s-<down>"
  '(dwl:focus-monitor 'DIRECTION-DOWN)
  "s-S-<left>"
  '(dwl:tag-monitor 'DIRECTION-LEFT)
  "s-S-<right>"
  '(dwl:tag-monitor 'DIRECTION-RIGHT)
  "s-s"
  '(dwl:spawn
     "notify-send"
     "Power management"
     "Reboot (r)\n Suspend (s)\n Hibernate (h)\n Poweroff (p)")
  "s-s r"
  '((lambda ()
      (dwl:spawn "notify-send" "System" "Reboot")
      (dwl:spawn "loginctl" "reboot")))
  "s-s h"
  '((lambda ()
      (dwl:spawn "notify-send" "System" "Hibernate")
      (dwl:spawn "loginctl" "hibernate")))
  "s-s p"
  '((lambda ()
      (dwl:spawn "notify-send" "System" "Poweroff")
      (dwl:spawn "loginctl" "poweroff")))
  "s-s s"
  '((lambda ()
      (dwl:spawn "notify-send" "System" "Suspend")
      (dwl:spawn "loginctl" "suspend")))
  "s-S-<up>"
  '(dwl:tag-monitor 'DIRECTION-UP)
  "s-S-<down>"
  '(dwl:tag-monitor 'DIRECTION-DOWN)
  "s-C-1"
  '(dwl:toggle-view 1)
  "s-C-2"
  '(dwl:toggle-view 2)
  "s-C-3"
  '(dwl:toggle-view 3)
  "s-C-4"
  '(dwl:toggle-view 4)
  "s-C-5"
  '(dwl:toggle-view 5)
  "s-C-6"
  '(dwl:toggle-view 6)
  "s-C-7"
  '(dwl:toggle-view 7)
  "s-C-8"
  '(dwl:toggle-view 8)
  "s-C-9"
  '(dwl:toggle-view 9)
  "s-q"
  'dwl:kill-client
  "s-r"
  'dwl:reload-config
  "s-<space>"
  'dwl:zoom
  "s-<tab>"
  'dwl:view-previous
  "s-0"
  '(dwl:view 0)
  "s-f"
  'dwl:toggle-fullscreen
  "S-s-<space>"
  'dwl:toggle-floating
  "S-s-<escape>"
  'dwl:quit
  "<XF86PowerOff>"
  'dwl:quit
  "s-<mouse-left>"
  'dwl:move
  "s-<mouse-middle>"
  'dwl:toggle-floating
  "s-<mouse-right>"
  'dwl:resize
  "s-<return>"
  '(dwl:spawn "footclient")
  "s-<escape>"
  '(dwl:spawn "swaylock" "-C" "/home/k8x1d/.config/swaylock/config")
  "s-i"
  '(dwl:spawn "idle_toggle")
  "<XF86MonBrightnessUp>"
  '(dwl:spawn "light" "-A" "2.5")
  "<XF86MonBrightnessDown>"
  '(dwl:spawn "light" "-U" "2.5")
  "<XF86AudioRaiseVolume>"
  '(dwl:spawn "pactl" "set-sink-volume" "@DEFAULT_SINK@" "+2.5%")
  "<XF86AudioLowerVolume>"
  '(dwl:spawn "pactl" "set-sink-volume" "@DEFAULT_SINK@" "-2.5%")
  "<XF86AudioMute>"
  '(dwl:spawn "pactl" "set-sink-mute" "@DEFAULT_SINK@" "toggle")
  "<XF86AudioMicMute>"
  '(dwl:spawn "pactl" "set-source-mute" "@DEFAULT_SOURCE@" "toggle")
  "<XF86AudioPrev>"
  '(dwl:spawn "mpc" "prev")
  "<XF86AudioNext>"
  '(dwl:spawn "mpc" "next")
  "<XF86AudioPlay>"
  '(dwl:spawn "mpc" "toggle"))
(set-layouts 'default "[]=" 'dwl:tile 'monocle "|M|" 'dwl:monocle)
(set-xkb-rules '((layout . "ca") (options . "ctrl:nocaps")))
(set-rules
  '((id . "emacs") (tags . 2))
  '((id . "thunderbird") (floating . #t) (tags . 3))
  '((id . "signal") (floating? . #f) (tags . 4))
  '((id . "signal-desktop") (floating? . #f) (tags . 4))
  '((id . "Signal") (floating? . #f) (tags . 4))
  '((title . "My Library.+") (floating? . #f) (tags . 5))
  '((title . "Quick.+") (floating? . #t))
  '((id . "nyxt") (floating? . #f) (tags . 1))
  '((id . "org.telegram.desktop") (floating? . #t) (tags . 4))
  '((id . "wlogout") (floating? . #f) (tags . 0))
  '((id . "firefox") (tags . 1)))
(set-monitor-rules
  '((name . "eDP-1")
    (masters . 1)
    (master-factor . 0.55)
    (scale . 1.5)
    (x . 0)
    (y . 0)
    (width . 2256)
    (height . 1504)
    (layout . default))
  '((name . "DP-2") (x . 2256) (y . 0)))
(add-hook!
  dwl:hook-startup
  (lambda ()
    (dwl:start-repl-server)
    (dwl:shcmd
      "swaybg"
      "-m"
      "fill"
      "-i"
      "/home/k8x1d/Pictures/wallpapers/paintings/Ryck_Rudd/Study_for_Portrait_of_Egon_Schiele.jpg")
    (dwl:spawn "/home/k8x1d/.nix-profile/bin/protonmail-bridge" "-n")
    (dwl:spawn "redshift" "-c" "/home/k8x1d/.config/redshift/redshift.conf")
    (dwl:spawn
      "/home/k8x1d/.nix-profile/bin/languagetool-http-server"
      "--config"
      "/home/k8x1d/.config/LanguageTool/config"
      "--port"
      "8081"
      "--allow-origin"
      "*")
    (dwl:spawn "light" "-S" "2")
    (dwl:spawn "dunst")
    (dwl:spawn "swayidle" "-C" "/home/k8x1d/.config/swayidle/config")
    (dwl:spawn ".nextcloud-real" "--background")
    (dwl:spawn "gnome-keyring-daemon" "--start" "--components=secrets")
    (dwl:spawn
      "signal-desktop"
      "--enable-features=UseOzonePlatform"
      "--ozone-platform=wayland")
    (dwl:spawn "k8x1d-emacs" "-d")
    (dwl:spawn "foot" "--term=alacritty" "--server")
    (dwl:spawn
      "/home/k8x1d/.guix-home/profile/libexec/polkit-kde-authentication-agent-1")
     (dwl:spawn "mpDris2")
     (dwl:spawn "mpris-proxy")
    ))
