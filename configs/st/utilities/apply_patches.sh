#!/bin/sh
patch -i patches/st-alpha-20220206-0.8.5.diff

patch -i patches/st-delkey-20201112-4ef0cbd.diff

patch -i patches/st-gruvbox-dark-0.8.5.diff

patch -i patches/st-scrollback-20210507-4536f46.diff
patch -i patches/st-scrollback-mouse-20220127-2c5edf2.diff
patch -i patches/st-scrollback-mouse-altscreen-20220127-2c5edf2.diff
