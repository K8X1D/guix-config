#!/bin/sh
mkdir patches
cd patches

wget https://st.suckless.org/patches/alpha/st-alpha-20220206-0.8.5.diff

wget https://st.suckless.org/patches/delkey/st-delkey-20201112-4ef0cbd.diff

wget https://st.suckless.org/patches/gruvbox/st-gruvbox-dark-0.8.5.diff

wget https://st.suckless.org/patches/scrollback/st-scrollback-20210507-4536f46.diff
wget https://st.suckless.org/patches/scrollback/st-scrollback-mouse-20220127-2c5edf2.diff
wget https://st.suckless.org/patches/scrollback/st-scrollback-mouse-altscreen-20220127-2c5edf2.diff
