#!/bin/sh
xset r rate 300 50
xinput set-prop 'PNP0C50:00 06CB:CD79 Touchpad' 'libinput Tapping Enabled' 1
redshift -l "45.50884:-73.58781" -t "6500:3000" &

# Gtk theme
THEME_VARIANT=Dark
export THEME_VARIANT
#export GTK_THEME=Breeze-$THEME_VARIANT
export GTK_THEME=Breeze-Dark

# Add lock screen to monitor turn off and sleep
Locker='slock && sleep 1'
xss-lock --transfer-sleep-lock -- $Locker --nofork &

# Set monitor turn off timer + prevent suspend
#xset s off
#xset dpms 300 0 0 # doesn't stop going to hibernate... remove for now
xset s off -dpms

# fix initial brightness
brightnessctl s 1 

# Launch users services
shepherd

exec dwm
